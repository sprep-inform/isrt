<?php

/**
* Settings for running the site in a local dev docker environment.
* Matches the docker config from docker-compose.yml in the project root
**/

$databases['default']['default'] = array(
  'driver' => getenv('DB_DRIVER'),
  'database' => getenv('DB_NAME'),
  'username' => getenv('DB_USER'),
  'password' => getenv('DB_PASSWORD'),
  'host' => getenv('DB_HOST')
);

// filesystem settings
$settings['file_private_path'] = '/var/www/html/docker-init/files/private';
// $settings['file_public_path'] = DRUPAL_ROOT . '/sites/default/files';
// $config['system.file']['path']['temporary'] = '/tmp';

// Turn off debugging etc.
//$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';
//$settings['cache']['bins']['render'] = 'cache.backend.null';
//$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
//$config['system.logging']['error_level'] = 'verbose';

/**
 * Disable CSS and JS aggregation in development
 */
//$config['system.performance']['css']['preprocess'] = FALSE;
//$config['system.performance']['js']['preprocess'] = FALSE;

// #redis config
// $conf['redis_client_host'] = 'redis';
// $conf['redis_client_interface'] = 'PhpRedis';
// $conf['lock_inc'] = '/sites/default/modules/contrib/redis/redis.lock.inc';
// $conf['path_inc'] = '/sites/default/modules/contrib/redis/redis.path.inc';
// $conf['cache_backends'][] = '/sites/default/modules/contrib/redis/redis.autoload.inc';
// $conf['cache_default_class'] = 'Redis_Cache';
// $conf['cache_class_cache_form'] = 'DrupalDatabaseCache';


// #varnish config. Matches settings in docker-compose.yml
// $conf['varnish_bantype'] = '0';
// $conf['varnish_cache_clear'] = '1';
// $conf['varnish_control_key'] = 'secret';
// $conf['varnish_control_terminal'] = 'varnish:6082';
// $conf['varnish_socket_timeout'] = 100;
// $conf['varnish_version'] = "4";

// config settings
$settings['config_sync_directory'] = '../config/sync';

// Turn off debugging etc.
//$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';
//$settings['cache']['bins']['render'] = 'cache.backend.null';
//$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';

// Turn off SSL verify etc.
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/eopts.development.services.yml';

$settings['skip_permissions_hardening'] = TRUE;

// Devel config
$config['devel.settings']['devel_dumper'] = 'var_dumper';

/**
 * Trusted host settings for local dev
 */
$settings['trusted_host_patterns'] = array(
  '^localhost$',
  '^nginx$',
  '^indicator-reporting-tool.localhost$'
);