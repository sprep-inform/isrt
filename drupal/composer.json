{
    "name": "drupal/recommended-project",
    "description": "Project template for Drupal 9 projects with a relocated document root",
    "type": "project",
    "license": "GPL-2.0-or-later",
    "homepage": "https://www.drupal.org/project/drupal",
    "support": {
        "docs": "https://www.drupal.org/docs/user_guide/en/index.html",
        "chat": "https://www.drupal.org/node/314178"
    },
    "repositories": {
        "0": {
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        },
        "chosen": {
            "type": "package",
            "package": {
                "name": "harvesthq/chosen",
                "version": "1.8.7",
                "type": "drupal-library",
                "dist": {
                    "url": "https://github.com/jjj/chosen/releases/download/v1.8.2/chosen_v1.8.2.zip",
                    "type": "zip"
                }
            }
        }
    },

    "require": {
        "composer/installers": "^1.9",
        "cweagans/composer-patches": "^1.7",
        "drupal/admin_toolbar": "^3.0",
        "drupal/chosen": "^3.0",
        "drupal/coder": "^8.3",
        "drupal/components": "^3.0.0@beta",
        "drupal/core-composer-scaffold": "^10",
        "drupal/core-project-message": "^10",
        "drupal/core-recommended": "^10",
        "drupal/embed": "^1.0",
        "drupal/entity_embed": "^1.0",
        "drupal/google_analytics": "^4.0",
        "drupal/group": "^1.6",
        "drupal/image_base64_formatter": "^2.0",
        "drupal/inline_entity_form": "^1.0-rc3",
        "drupal/media_entity_browser": "^2.0@alpha",
        "drupal/radix": "^4.13",
        "drupal/seven": "^1.0",
        "drupal/stable": "^2.0",
        "drush/drush": "^12",
        "harvesthq/chosen": "^1.8",
        "phpcompatibility/php-compatibility": "^9.3",
        "phpspec/prophecy-phpunit": "^2",
        "vlucas/phpdotenv": "^5.2",
        "webflo/drupal-finder": "^1.2",
        "webmozart/path-util": "^2.3"
    },
    "require-dev": {
        "behat/mink-browserkit-driver": "^2.1",
        "behat/mink-selenium2-driver": "^1.6",
        "dmore/chrome-mink-driver": "^2.7",
        "drupal/devel_kint_extras": "^1.0",
        "mglaman/drupal-check": "^1.1",
        "symfony/browser-kit": "^6.2",
        "symfony/phpunit-bridge": "^6.0",
        "weitzman/drupal-test-traits": "^2.0"
    },
    "conflict": {
        "drupal/drupal": "*"
    },
    "minimum-stability": "dev",
    "prefer-stable": true,
    "config": {
        "sort-packages": true,
        "allow-plugins": {
            "composer/installers": true,
            "cweagans/composer-patches": true,
            "dealerdirect/phpcodesniffer-composer-installer": true,
            "drupal/core-composer-scaffold": true,
            "drupal/core-project-message": true
        }
    },
    "autoload": {
        "classmap": [
            "scripts/composer/ScriptHandler.php"
        ]
    },
    "scripts": {
        "pre-install-cmd": [
            "DrupalProject\\composer\\ScriptHandler::checkComposerVersion"
        ],
        "pre-update-cmd": [
            "DrupalProject\\composer\\ScriptHandler::checkComposerVersion"
        ],
        "post-install-cmd": [
            "DrupalProject\\composer\\ScriptHandler::createRequiredFiles",
            "@remove_git_directories"
        ],
        "post-update-cmd": [
            "DrupalProject\\composer\\ScriptHandler::createRequiredFiles",
            "@remove_git_directories"
        ],
        "post-drupal-scaffold-cmd": [
            "echo 'skipping patch -p1 < patches/htaccess.patch'"
        ],
        "remove_git_directories": [
            "find /var/www/html/drupal -name '.git' | xargs rm -rf"
        ],
        "build": [
            "drush site:install minimal --existing-config --db-url=mysql://root:password@mariadb:3306/drupal -y",
            "drush ev 'irt_run_after_full_install()'",
            "drush cr",
            "drush cron",
            "drush php-eval 'node_access_rebuild()'"
        ],
        "run_tests": [
            "mkdir -p /tmp/b2b",
            "phpunit -c /var/www/html/drupal/phpunit.xml"
        ],
        "run_single_test_class": [
            "phpunit -c /var/www/html/drupal/phpunit.xml web/modules/custom/my_module/tests/src/Functional/MyTest.php"
        ],
        "php_compatibility": [
            "phpcs -p --extensions=php,module,theme,profile,inc,install /var/www/html/drupal/web/*/custom --standard=PHPCompatibility --runtime-set testVersion 8.2"
        ],
        "coding_standards": [
            "phpcs -p --extensions=php,module,theme,profile,inc,install /var/www/html/drupal/web/*/custom --standard=Drupal --ignore=*/node_modules/*"
        ],
        "coding_standards_fix": [
            "phpcbf -p --extensions=php,module,theme,profile,inc,install /var/www/html/drupal/web/*/custom --standard=Drupal --ignore=*/node_modules/*"
        ],
        "cs": [
            "@coding_standards"
        ],
        "csf": [
            "@coding_standards_fix"
        ],
        "upgrade_status_check": [
            "drush upgrade_status:analyze  --all --ignore-contrib"
        ],
        "copy_files_to_public_files_folder": [
            "cp -rf /var/www/html/docker-init/files/public/default_content_files /var/www/html/drupal/web/sites/default/files",
            "cp -rf /var/www/html/docker-init/files/public/embed_buttons /var/www/html/drupal/web/sites/default/files"
        ]
    },
    "extra": {
        "drupal-scaffold": {
            "locations": {
                "web-root": "web/"
            }
        },
        "patches": {
            "drupal/google_analytics" : {
                "Cannot install from existing config" : "https://www.drupal.org/files/issues/2024-01-22/google_analytics-3373921-15.patch"
            },
            "drupal/group": {
                "Add new user in the group- https://www.drupal.org/project/group/issues/2949408": "https://www.drupal.org/files/issues/2023-10-04/group-manage-users-2949408-56.patch",
                "deprecated function" : "https://www.drupal.org/files/issues/2023-07-25/3349565-6.patch"
            }
        },
        "installer-paths": {
            "web/core": [
                "type:drupal-core"
            ],
            "web/libraries/{$name}": [
                "type:drupal-library"
            ],
            "web/modules/contrib/{$name}": [
                "type:drupal-module"
            ],
            "web/profiles/contrib/{$name}": [
                "type:drupal-profile"
            ],
            "web/themes/contrib/{$name}": [
                "type:drupal-theme"
            ],
            "drush/Commands/contrib/{$name}": [
                "type:drupal-drush"
            ],
            "web/modules/custom/{$name}": [
                "type:drupal-custom-module"
            ],
            "web/profiles/custom/{$name}": [
                "type:drupal-custom-profile"
            ],
            "web/themes/custom/{$name}": [
                "type:drupal-custom-theme"
            ]
        },
        "drupal-core-project-message": {
            "include-keys": [
                "homepage",
                "support"
            ],
            "post-create-project-cmd-message": [
                "<bg=blue;fg=white>                                                         </>",
                "<bg=blue;fg=white>  Congratulations, you’ve installed the Drupal codebase  </>",
                "<bg=blue;fg=white>  from the drupal/recommended-project template!          </>",
                "<bg=blue;fg=white>                                                         </>",
                "",
                "<bg=yellow;fg=black>Next steps</>:",
                "  * Install the site: https://www.drupal.org/docs/8/install",
                "  * Read the user guide: https://www.drupal.org/docs/user_guide/en/index.html",
                "  * Get support: https://www.drupal.org/support",
                "  * Get involved with the Drupal community:",
                "      https://www.drupal.org/getting-involved",
                "  * Remove the plugin that prints this message:",
                "      composer remove drupal/core-project-message"
            ]
        }
    }
}
