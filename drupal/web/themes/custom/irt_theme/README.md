# Theme development

## Local setup for theming development

**This section needs to be done once only for your developer user account**

To do theme development you'll need to
[install Node.js](https://nodejs.org/en/download/).

Install gulp-cli as a global package

> $ npm install -g npm-cli

Also recommend `sass-lint` as a linter for the themes. It is an npm package, and
you'll need to configure your IDE to use it for sass files.

> $ npm install -g sass-lint


## Developing in this theme

Inside your created theme folder

> $ cd irt_theme
> $ npm install

If you do not end up with a `node_modules` folder containing `bootstrap` amongst
hundreds of other folders, run this command:

> $ sh npm-install.sh

Then you are good to go! (hopefully)

> $ gulp

A browser-sync window will open that dynamically updates as you change the theme
files.

### Test is works

You won't be logged in in the browser-sync window.

- Under the Drupal appearance settings choose 'IRT Theme'
and 'Install and set default'.

- Refreshing the page will show an attractive yellow background.

- Go to `sass/_variables.sass` and change the body-bg variable to another color.

- The browser-sync window (port 3000 probably) will update as soon as the css is
compiled.
