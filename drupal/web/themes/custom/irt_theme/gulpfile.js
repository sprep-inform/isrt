"use strict";

// Pluygins
const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const plumber = require('gulp-plumber');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;
const notify = require('gulp-notify');
const cssnano = require('cssnano');
const postcss = require('gulp-postcss');
const spawn = require("child_process").spawn;
var svgstore = require('gulp-svgstore');
var svgmin = require('gulp-svgmin');
var path = require('path');


var paths = {
  styles: {
    src: 'sass/**/*.sass',
    dest: 'css/'
  },
  theme: {
    root: './',
    templates: 'templates/**/*.html.twig',
    layouts: 'layouts/**/*.html.twig',
    theme: '*.theme',
    yml: '*.yml'
  }
};

/////////////////////////////////////////////////////////////////////////////
// Tasks

// Move the javascript files into our js folder
function js() {
  return gulp.src([
    'node_modules/bootstrap/dist/js/bootstrap.min.js',
    'node_modules/popper.js/dist/umd/popper.min.js'])
        .pipe(gulp.dest("js"))
        .pipe(browserSync.stream());
}

// Convert any svg files in images/icons into an svg sprite
function makesprites() {
  return gulp
    .src('images/icons/*.svg')
    .pipe(svgmin(function(file) {
      var prefix = path.basename(file.relative, path.extname(file.relative));
      return {
        plugins: [{
          cleanupIDs: {
            prefix: prefix + '-',
            minify: true
          }
        }]
      };
    }))
    .pipe(svgstore())
    .pipe(gulp.dest('images/sprites'));
}


// Styles
function styles() {
  return gulp.src(
    [paths.styles.src]
  )
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.identityMap())
    .pipe(
      sass({
        includePaths: [paths.styles.src]
      })
        .on('error', function(err) {
          notify().write(err);
          this.emit('end');
        })
    )
    .pipe(plumber())
    .pipe(
      postcss([
        autoprefixer(),
        cssnano()
      ])
    )
    .pipe(sourcemaps.write('.'))
    .pipe(
      gulp.dest(paths.styles.dest)
    )
    .pipe(
      browserSync.stream()
    );
};

// browsersync daemon
function browser_sync() {
  browserSync.init({
    proxy: 'http://indicator-reporting-tool.localhost',
    https: false
  });
};

// drupal cache rebuild
function drushcr(cb) {
  const child = spawn(
    'docker-compose',
    ['exec', '-T', 'php', 'bash', '-c',
     'cd /var/www/html/drupal/web && drush cr']
  );
  child.stderr.on(
    'data',
    function(data) {
      console.log(data.toString());
    }
  );
  child.stdout.on(
    'data',
    function(data) {
      console.log(data.toString());
    }
  );
}


gulp.task('default', function() {
  js();
  styles();
  browser_sync();
  gulp.watch(paths.styles.src, gulp.series(styles));
  gulp.watch(paths.theme.templates, gulp.series(drushcr));
  gulp.watch(paths.theme.layouts, gulp.series(drushcr));
  gulp.watch(paths.theme.theme, gulp.series(drushcr));
  gulp.watch(paths.theme.yml, gulp.series(drushcr));
  return;
});
