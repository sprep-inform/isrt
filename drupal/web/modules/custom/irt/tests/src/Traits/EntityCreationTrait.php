<?php

namespace Drupal\Tests\irt\Traits;

use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\user\Entity\User;

/**
 * The Entity Creation Trait helper.
 */
trait EntityCreationTrait {

  /**
   * Helper function to create a group.
   *
   * @param string $name
   *   Group Name.
   * @param string $type
   *   The group type, by default for now its 'countries'.
   *
   * @return \Drupal\Core\Entity\EntityInterface|mixed|null
   *   Returns the group object entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function createGroup($name, $type = 'countries') {

    $values = [
      'label' => $name,
      'type' => $type,
    ];
    $group = NULL;
    $existingGroup = \Drupal::entityTypeManager()
      ->getStorage('group')
      ->loadByProperties(['label' => $name]);
    if ($existingGroup) {
      $group = current($existingGroup);
    }
    else {
      $group = Group::create($values);
      $group->save();
    }

    return $group;
  }

  /**
   * Helper to create a user in a group.
   *
   * @param string $username
   *   The drupal username.
   * @param string $groupName
   *   The group name.
   * @param array $group_roles
   *   Group roles.
   * @param string $groupType
   *   The group type, by default for now its 'countries'.
   *
   * @return mixed
   *   Returns an array [0 => user object, 1 => group ID].
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createGroupUser($username, $groupName, array $group_roles = [], $groupType = 'countries') {

    $group = NULL;
    $existingGroup = \Drupal::entityTypeManager()
      ->getStorage('group')
      ->loadByProperties(['label' => $groupName]);
    if ($existingGroup) {
      $group = reset($existingGroup);
    }
    else {
      $group = $this->createGroup($groupName, $groupType);
    }

    $user = $this->createUserWithRoles($username, []);
    $this->createMembership($user, $groupType, $group->id(), $group_roles);
    $user->passRaw = 'password';
    return ['user' => $user, 'group' => $group];
  }

  /**
   * Helper to create a Drupal user with roles.
   *
   * @param string $username
   *   The drupal username.
   * @param array $roles
   *   The drupal roles[].
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\user\Entity\User|mixed|null
   *   Returns the user object;
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createUserWithRoles($username, array $roles = []) {
    $user = NULL;
    $edit = [];
    $edit['name'] = $username;
    $edit['mail'] = $edit['name'] . '@example.com';
    $edit['status'] = 1;
    $edit['pass'] = 'password';

    $existing = current(\Drupal::entityTypeManager()
      ->getStorage('user')
      ->loadByProperties(['name' => $username]));

    if ($existing) {
      $user = $existing;
    }
    else {
      $user = User::create($edit);
      $user->save();
      foreach ($roles as $role) {
        $user->addRole($role);
        $user->save();
      }
    }
    $this->assertNotNull($user, "User created/loaded shouldn't be null.");
    $user->passRaw = $edit['pass'];
    return $user;
  }

  /**
   * Helper function to create a group-user membership.
   *
   * @param \Drupal\user\Entity\User $user
   *   Drupal user.
   * @param string $groupType
   *   Group type.
   * @param int $groupId
   *   Group id.
   * @param array $group_roles
   *   Passing a Group role (not Drupal role).
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\group\Entity\GroupContent|mixed|null
   *   Returns a ContentEntity object (bundle membership)
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createMembership(User $user, $groupType, $groupId, array $group_roles) {
    $properties = [
      'type' => "$groupType-group_membership",
      'entity_id' => $user->id(),
      'gid' => $groupId,
    ];
    $membership = NULL;
    $existing = current(\Drupal::entityTypeManager()
      ->getStorage('group_content')
      ->loadByProperties($properties));

    if ($existing) {
      $membership = $existing;
    }
    else {
      $properties = $properties + ['label' => $user->label()];
      $membership = GroupContent::create($properties);
      $membership->set('group_roles', $group_roles);
      $membership->save();
    }
    return $membership;
  }

}
