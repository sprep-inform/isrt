<?php

namespace Drupal\Tests\irt\Traits;

use Behat\Mink\Exception\UnsupportedDriverActionException;
use Drupal\Core\File\FileSystemInterface;

/**
 * Trait UtilityTrait is the utility trait.
 *
 * @package Drupal\Tests\irt\Traits
 */
trait UtilityTrait {

  /**
   * Screen shotter.
   *
   * @param int $width
   *   Width in pixels.
   * @param int $height
   *   Height in pixels.
   */
  public function takeScreenShot($width = 720, $height = 1024) {

    /** @var \Behat\Mink\Session $session */
    $session = $this->getSession();

    try {
      $session->resizeWindow($width, $height);
    }
    catch (UnsupportedDriverActionException $ex) {
      return;
    }

    $class = str_replace("\\", "_", __CLASS__);

    $path = $this->htmlOutputDirectory . '/screenshots';
    \Drupal::service('file_system')->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);

    $counter_file = "$path/$class.counter";
    if (file_exists($counter_file)) {
      $counter = file_get_contents($counter_file);
    }
    else {
      $counter = 1;
    }
    file_put_contents($counter_file, $counter + 1);

    $filename = $class . '-' . $counter . '.png';
    file_put_contents("$path/$filename", $session->getScreenshot());
  }

  /**
   * Helper to fill Wysiwyg field.
   *
   * @param string $locator
   *   String locator.
   * @param string $text
   *   String value.
   *
   * @throws \Exception
   */
  public function iFillInTheWysiwygEditor($locator, $text) {
    // Find the field using the locator.
    $field = $this->getSession()->getPage()->findField($locator);

    // Get the ID of the field (this should correspond to the editor ID)
    $id = $field->getAttribute('id');

    // Use the CKEditor instance API to set the editor data.
    $this->getSession()->executeScript("
        const editor = Drupal.CKEditor5Instances.get('$id');
        if (editor) {
            editor.setData('$text');
        }
    ");
  }

  /**
   * Get the wysiwyg instance variable to use in Javascript.
   *
   * @param string $instanceId
   *   The instanceId used by the WYSIWYG module to identify the instance.
   *
   * @return string
   *   A Javascript expression representing the WYSIWYG instance.
   *
   * @throws \Exception
   */
  protected function getWysiwygInstance($instanceId) {
    $instance = "CKEDITOR.instances['$instanceId']";
    if (!$this->getSession()->evaluateScript("return !!$instance")) {
      throw new \Exception(sprintf('The editor "%s" was not found on the page %s', $instanceId, $this->getSession()
        ->getCurrentUrl()));
    }
    return $instance;
  }

  /**
   * Inspect if a node exists.
   *
   * @param array $properties
   *   The props of a node.
   *
   * @return mixed|null
   *   Returns the node object or null.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function nodeExists(array $properties) {
    $node = NULL;
    $existing = current(\Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadByProperties($properties));

    if ($existing) {
      $node = $existing;
    }
    return $node;
  }

}
