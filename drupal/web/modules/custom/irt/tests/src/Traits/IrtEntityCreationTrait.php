<?php

namespace Drupal\Tests\irt\Traits;

use Drupal\group\Entity\Group;

/**
 * Trait IrtEntityCreationTrait is the Irt Entity Creation Trait.
 *
 * @package Drupal\Tests\irt\Traits
 */
trait IrtEntityCreationTrait {

  /**
   * Helper function to create a node of bundle indicator_definition.
   *
   * @param \Drupal\group\Entity\Group $group
   *   The group object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function createIndicatorDefinitionGroupContent(Group $group) {
    $targets = ['Indicator Definition Test 1', 'Indicator Definition Test 2'];
    foreach ($targets as $title) {
      if ($this->nodeExists(['title' => $title])) {
        continue;
      }
      $web_assert = $this->assertSession();
      $contentAddUrl = "/group/{$group->id()}/content/create/group_node:indicator_definition";
      $this->visit($contentAddUrl);
      $this->htmlOutput();
      $web_assert->pageTextNotContains('Access denied');
      /** @var \Behat\Mink\Element\TraversableElement $page */
      $page = $this->getCurrentPage();

      $page->fillField('title[0][value]', $title);
      $page->fillField('field_definition[0][value]', "field_definition[0][value]: The brown fox jumped over the lazy dog.");
      $page->fillField('field_desired_outcome[0][value]', "field_desired_outcome: The brown fox jumped over the lazy dog.");
      $page->fillField('field_measurement_or_calc[0][value]', "field_measurement_or_calc:  The brown fox jumped over the lazy dog.");
      $page->fillField('field_unit_of_measurement[0][value]', "field_unit_of_measurement:  The brown fox jumped over the lazy dog.");
      $page->fillField('field_rationale_assumptions[0][value]', "field_rationale_assumptions:  The brown fox jumped over the lazy dog.");
      $page->fillField('field_preferred_data_source[0][value]', "field_preferred_data_source:  The brown fox jumped over the lazy dog.");
      $this->htmlOutput();

      $submit_button = $page->findButton('Save');
      $this->assertNotNull($submit_button, "Button object shouldn't be null");
      $submit_button->press();
      $this->takeScreenShot();
      $this->htmlOutput();
    }
  }

  /**
   * Helper function to create a node of bundle indicator_state.
   *
   * @param \Drupal\group\Entity\Group $group
   *   The group object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function createIndicatorStateGroupContent(Group $group) {
    $targets = ['Indicator State Test 1'];
    foreach ($targets as $i => $title) {
      if ($this->nodeExists(['title' => $title])) {
        continue;
      }
      $web_assert = $this->assertSession();
      $contentAddUrl = "/group/{$group->id()}/content/create/group_node:indicator_state";
      $this->visit($contentAddUrl);
      $this->htmlOutput();
      $web_assert->pageTextNotContains('Access denied');
      /** @var \Behat\Mink\Element\TraversableElement $page */
      $page = $this->getCurrentPage();

      $page->fillField('title[0][value]', $title);
      $page->fillField('field_year_valid[0][value][date]', date('Y-m-d', strtotime('now')));
      $page->fillField('field_state_indicator_definition[0][target_id]', "Indicator Definition Test 1");
      $page->selectFieldOption('field_indicator_assessment', "GOOD-IMPROVING-HIGH");
      $this->iFillInTheWysiwygEditor('field_key_findings[0][value]', "Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Pellentesque in ipsum id orci porta dapibus. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit.");
      $this->iFillInTheWysiwygEditor('field_status_narrative[0][value]', "Status Narrative:  The brown fox jumped over the lazy dog.");
      $this->iFillInTheWysiwygEditor('field_impact[0][value]', "Impact:  The brown fox jumped over the lazy dog.");
      $this->iFillInTheWysiwygEditor('field_response_recommendations[0][value]', "Response and Recommendations:  The brown fox jumped over the lazy dog.");
      $this->iFillInTheWysiwygEditor('field_sources[0][value]', "Sources:  The brown fox jumped over the lazy dog.");
      $this->htmlOutput();

      $submit_button = $page->findButton('Save');
      $this->assertNotNull($submit_button, "Button object shouldn't be null");
      $submit_button->press();
      $this->takeScreenShot();
      $this->htmlOutput();
    }
  }

  /**
   * Helper function to create a node of bundle target.
   *
   * @param \Drupal\group\Entity\Group $group
   *   The group object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function createTargetGroupContent(Group $group) {
    // Set how many nodes with titles.
    $targets = [
      ['Target Test 10 def-1', 'Target Test 20 def-2'],
      ['Target Test 11 def-1', 'Target Test 21 def-2'],
    ];
    foreach ($targets as $i => $row) {
      foreach ($row as $x => $title) {
        if ($this->nodeExists(['title' => $title])) {
          continue;
        }
        $web_assert = $this->assertSession();
        $contentAddUrl = "/group/{$group->id()}/content/create/group_node:target";
        $this->visit($contentAddUrl);
        $this->htmlOutput();
        $web_assert->pageTextNotContains('Access denied');
        /** @var \Behat\Mink\Element\TraversableElement $page */
        $page = $this->getCurrentPage();
        $page->fillField('title[0][value]', $title);
        $this->iFillInTheWysiwygEditor('body[0][value]', "body[0][value]:  The brown fox jumped over the lazy dog.");

        // Testing inline entity.
        $b1 = $page->findButton('Add existing Indicator Definition');
        $this->takeScreenShot(720, 1200);
        $this->assertNotNull($b1, "Button object shouldn't be null");
        $b1->press();
        $page->waitFor(2, function () use ($page) {
          return !empty($page->find('xpath', 'field_indicator_definition[form][0][entity_id]'));
        });
        $page->fillField('field_indicator_definition[form][0][entity_id]', "Indicator Definition Test " . ($i + 1) . " (" . ($i + 1) . ")");
        $this->htmlOutput();

        $submit_button = $page->findButton('Save');
        $this->assertNotNull($submit_button, "Button object shouldn't be null");
        $submit_button->press();
        $this->takeScreenShot();
        $this->htmlOutput();
      }
    }
  }

  /**
   * Helper function to create a node of bundle obligation.
   *
   * @param \Drupal\group\Entity\Group $group
   *   The group object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function createObligationGroupContent(Group $group) {
    // Set how many nodes with titles.
    $targets = ['Obligation Test 1'];
    foreach ($targets as $i => $title) {
      if ($this->nodeExists(['title' => $title])) {
        continue;
      }
      $web_assert = $this->assertSession();
      $contentAddUrl = "/group/{$group->id()}/content/create/group_node:obligation";
      $this->visit($contentAddUrl);
      $this->htmlOutput();
      $web_assert->pageTextNotContains('Access denied');
      /** @var \Behat\Mink\Element\TraversableElement $page */
      $page = $this->getCurrentPage();
      $page->fillField('title[0][value]', $title);
      $this->iFillInTheWysiwygEditor('body[0][value]', "body[0][value]:  The brown fox jumped over the lazy dog.");
      $submit_button = $page->findButton('Save');
      $this->assertNotNull($submit_button, "Button object shouldn't be null");
      $submit_button->press();
      $this->htmlOutput();
    }
  }

}
