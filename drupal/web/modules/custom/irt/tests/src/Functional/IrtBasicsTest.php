<?php

namespace Drupal\Tests\irt\Functional;

use Drupal\Core\Url;

use Drupal\Tests\irt\Traits\EntityCreationTrait;
use Drupal\Tests\irt\Traits\IrtEntityCreationTrait;
use Drupal\Tests\irt\Traits\UtilityTrait;
use Drupal\user\Entity\User;
use weitzman\DrupalTestTraits\ExistingSiteSelenium2DriverTestBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group irt
 */
class IrtBasicsTest extends ExistingSiteSelenium2DriverTestBase {

  use EntityCreationTrait;
  use IrtEntityCreationTrait;
  use UtilityTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['irt'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'irt_theme';

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $admin = User::load(1);
    $admin->passRaw = 'password';
    $this->drupalLogin($admin);
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testBasics() {
    // Make sure front page returns 200.
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->takeScreenshot();
    $this->assertSession()->pageTextContains('My account');
    $this->assertSession()->pageTextNotContains('Page not found');

    // Make sure non existing page returns 404.
    $this->drupalGet('/return404');
    $this->assertSession()->pageTextContains('Page not found');

    // Create a group.
    $group = $this->createGroup('Cook Islands');
    // Create admin user.
    $user_group = $this->createGroupUser('admin-cook-islands',
          'Cook Islands', ['countries-group_admin']);
    /** @var \Drupal\user\Entity\User $user */
    $groupAdminUser = $user_group['user'];
    // Login using the group admin.
    $this->drupalLogin($groupAdminUser);
    $this->assertSession()->waitForElementVisible('css', 'body');
    $this->assertSession()->pageTextContains('My account');
    $this->assertSession()->pageTextNotContains('Page not found');

    // Visit add member page as an admin.
    $this->visit("/group/{$group->id()}/members");
    $page = $this->getCurrentPage();
    $page->hasLink('Add existing member');
    $page->clickLink('Add existing member');
    $this->htmlOutput($page->getHtml());

    $this->drupalLogout();
    // Create regular user for the same group.
    $user_group = $this->createGroupUser('member-cook-islands',
          'Cook Islands', []);
    /** @var \Drupal\user\Entity\User $user */
    $groupMemberUser = $user_group['user'];
    // Login using the group admin.
    $this->drupalLogin($groupMemberUser);
    $web_assert = $this->assertSession();
    $this->assertSession()->waitForElementVisible('css', 'body');
    $this->assertSession()->pageTextContains('My account');
    $this->assertSession()->pageTextNotContains('Page not found');

    // Visit add member page as an member.
    // Should return access denied.
    $this->visit("/group/{$group->id()}/members");
    $this->htmlOutput($page->getHtml());
    $this->assertSession()->pageTextContains('Access denied');
  }

  /**
   * Tests that that creates basic nodes.
   */
  public function testCreateBasicContent() {
    // Login.
    $member_group = $this->createGroupUser('admin-cook-islands',
          'Cook Islands', ['countries-group_admin']);

    $admin_group = $this->createGroupUser('admin-cook-islands',
          'Cook Islands', []);

    /** @var \Drupal\user\Entity\User $user */
    $groupAdminUser = $member_group['user'];

    // Login using the group admin.
    $this->drupalLogin($groupAdminUser);
    $this->assertSession()->waitForElementVisible('css', 'body');
    $this->assertSession()->pageTextContains('My account');
    $this->assertSession()->pageTextNotContains('Page not found');
    $page = $this->getCurrentPage();
    $page->clickLink('My account');
    $this->htmlOutput();
    // Create indicator_definition .
    $this->createIndicatorDefinitionGroupContent($member_group['group']);

    // Create indicator_state .
    $this->createIndicatorStateGroupContent($member_group['group']);

    // Create an obligation and targets.
    $this->createObligationGroupContent($member_group['group']);
    $this->createTargetGroupContent($member_group['group']);
  }

  /**
   * Making sure basic links working.
   */
  public function testClickables() {
    $admin = User::load(1);
    $admin->passRaw = 'password';
    $this->drupalLogin($admin);
    $web_assert = $this->assertSession();
    $this->drupalGet('/group/1/home');
    $this->assertSession()->pageTextContains('Example structure of an indicator based report');
    $this->drupalGet('/irt/1/select_state_sheet');
    $this->assertSession()->pageTextContains('Produce Indicator State Sheet');
    $this->drupalGet('/irt/1/select_report');
    $this->assertSession()->pageTextContains('Producing a Report Step 1');
    $page = $this->getCurrentPage();
    $page->clickLink('Continue');
    $this->assertSession()->pageTextContains('Producing a Report Step 2');
    $this->drupalGet('/irt/1/state/step-1');
    $this->assertSession()->pageTextContains('Update Indicator State Step 1');
    $page = $this->getCurrentPage();
    $page->hasLink('Update Indicator State');
    // Don't know why clickLink not working here.
    /* $page->clickLink('Update Indicator State'); */
    $this->drupalGet('/irt/1/state/step-2/3');
    $this->drupalGet('/irt/1/state/step-1');
    $page = $this->getCurrentPage();
    $page->hasLink('Create Indicator State');
    // Don't know why clickLink not working here.
    /* $page->clickLink('Create Indicator State'); */
    $this->drupalGet('/irt/1/state/step-2/create/2');
    $this->assertSession()->pageTextContains('Create Indicator State Step 2');
  }

  /**
   * Creating target by obligation link.
   */
  public function createTargetGroupContentbyObligation() {
    $admin = User::load(1);
    $admin->passRaw = 'password';
    $this->drupalLogin($admin);
    $web_assert = $this->assertSession();
    $web_assert->statusCodeEquals(200);
    $this->visit("/irt/1/select_obligation");
    $this->htmlOutput();
    $this->takeScreenShot();
    $web_assert->statusCodeEquals(200);
    $web_assert->pageTextNotContains('Access denied');

    $this->clickLink("Manage Framework");
    $this->htmlOutput();
    $web_assert->statusCodeEquals(200);
    $web_assert->pageTextNotContains('Access denied');

    $this->clickLink("Create New Target");
    $this->htmlOutput();
    $web_assert->statusCodeEquals(200);
    $web_assert->pageTextNotContains('Access denied');

    /** @var \Behat\Mink\Element\TraversableElement $page */
    $page = $this->getCurrentPage();
    $page->fillField('title[0][value]', 'Test Target by obligation');
    $this->iFillInTheWysiwygEditor('body[0][value]', "body[0][value]:  The brown fox jumped over the lazy dog.");

    // Testing inline entity.
    $b1 = $page->findButton('Add existing Indicator Definition');
    $this->takeScreenShot(720, 1200);
    $this->assertNotNull($b1, "Button object shouldn't be null");
    $b1->press();
    $page->waitFor(2, function () use ($page) {
        return !empty($page->find('xpath', 'field_indicator_definition[0][entity_id]'));
    });
    $page->fillField('field_indicator_definition[0][entity_id]', "Indicator Definition Test ");
    $this->htmlOutput();

    $submit_button = $page->findButton('Save');
    $this->assertNotNull($submit_button, "Button object shouldn't be null");
    $submit_button->press();
    $this->takeScreenShot();
    $this->htmlOutput();

  }

}
