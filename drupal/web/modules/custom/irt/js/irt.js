/**
 * @file
 * Modules JS file.
 */

(function ($) {

  $('.irt-toolbar-text1').hide();
  $('.irt-toolbar-text2').hide();
  $('.irt-toolbar-text3').hide();
  $('.irt-toolbar-text4').hide();
  $('.irt-toolbar-text5').hide();

  $('.irt-toolbar-link1').click(function () {
    $('.irt-toolbar-text1').fadeIn(500);
    $('.irt-toolbar-text2').hide();
    $('.irt-toolbar-text3').hide();
    $('.irt-toolbar-text4').hide();
    $('.irt-toolbar-text5').hide();
  });
  $('.irt-toolbar-link2').click(function () {
    $('.irt-toolbar-text1').hide();
    $('.irt-toolbar-text2').fadeIn(500);
    $('.irt-toolbar-text3').hide();
    $('.irt-toolbar-text4').hide();
    $('.irt-toolbar-text5').hide();
  });
  $('.irt-toolbar-link3').click(function () {
    $('.irt-toolbar-text1').hide();
    $('.irt-toolbar-text2').hide();
    $('.irt-toolbar-text3').fadeIn(500);
    $('.irt-toolbar-text4').hide();
    $('.irt-toolbar-text5').hide();
  });
  $('.irt-toolbar-link4').click(function () {
    $('.irt-toolbar-text1').hide();
    $('.irt-toolbar-text2').hide();
    $('.irt-toolbar-text3').hide();
    $('.irt-toolbar-text4').fadeIn(500);
    $('.irt-toolbar-text5').hide();
  });
  $('.irt-toolbar-link5').click(function () {
    $('.irt-toolbar-text1').hide();
    $('.irt-toolbar-text2').hide();
    $('.irt-toolbar-text3').hide();
    $('.irt-toolbar-text4').hide();
    $('.irt-toolbar-text5').fadeIn(500);
  });

}(jQuery));
