<?php

namespace Drupal\irt\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\group\Entity\Group;
use Drupal\irt\UtilityTrait;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for Export document of state per obligation.
 */
class ExportStatePerObligationController extends ControllerBase {

  use UtilityTrait;

  /**
   * Produce exportable document.
   *
   * @param \Drupal\group\Entity\Group $group
   *   The group Object.
   * @param \Drupal\node\Entity\Node $node
   *   The Obligation node Object.
   */
  public function exportStatePerObligation(Group $group, Node $node) {
    $logger = $this->getLogger(__FUNCTION__);
    if ($node->bundle() != 'obligation') {
      $logger->warning("Should be an obligation node, got [{$node->bundle()}]!");
      throw new NotFoundHttpException();
    }

    header("Content-type: application/vnd.ms-word");
    header("Content-Disposition: attachment; filename= state_per_obligation.doc");
    $response = new Response();
    $view = views_embed_view('indicator_states_per_obligation', 'block_2');
    $response->setContent('<html>' . \Drupal::service('renderer')->render($view) . '</html>');
    $response->send();

    // Setting up cache.
    $cacheable_metadata = new CacheableMetadata();
    $cacheable_metadata->setCacheContexts(['user.group_permissions']);
    exit();
  }

}
