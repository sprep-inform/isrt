<?php

namespace Drupal\irt\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\group\Entity\Group;
use Drupal\irt\UtilityTrait;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for Export document of Indicator State.
 */
class ExportIndicatorStateController extends ControllerBase {

  use UtilityTrait;

  /**
   * Produce exportable document.
   *
   * @param \Drupal\group\Entity\Group $group
   *   The group Object.
   * @param \Drupal\node\Entity\Node $node
   *   The indicator_state node Object.
   */
  public function exportIndicatorState(Group $group, Node $node) {
    $logger = $this->getLogger(__FUNCTION__);
    if ($node->bundle() != 'indicator_state') {
      $logger->warning("Should be an indicator state node, got [{$node->bundle()}]!");
      throw new NotFoundHttpException();
    }

    header("Content-type: application/vnd.ms-word");
    header("Content-Disposition: attachment; filename= indicator_state.doc");
    $response = new Response();
    $view = views_embed_view('indicator_state', 'block_1');
    $response->setContent('<html>' . \Drupal::service('renderer')->render($view) . '</html>');
    $response->send();

    // Setting up cache.
    $cacheable_metadata = new CacheableMetadata();
    $cacheable_metadata->setCacheContexts(['user.group_permissions']);

    exit();
  }

}
