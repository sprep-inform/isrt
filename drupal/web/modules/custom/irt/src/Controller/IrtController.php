<?php

namespace Drupal\irt\Controller;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\group\Entity\Group;
use Drupal\irt\UtilityTrait;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for Social Group routes.
 */
class IrtController extends ControllerBase {

  use UtilityTrait;

  /**
   * The _title_callback for the entity.group.canonical route.
   *
   * @param \Drupal\group\Entity\Group $group
   *   The group Object.
   *
   * @return string
   *   The page title.
   */
  public function groupHomeTitle(Group $group) {
    return $group->label() . " Home page";
  }

  /**
   * The _title_callback for the entity.group.canonical route.
   *
   * @param \Drupal\group\Entity\Group $group
   *   The group Object.
   *
   * @return string
   *   The page title.
   */
  public function groupNodeFormTitle(Group $group) {
    $parameter = \Drupal::routeMatch()->getRawParameter('plugin_id');
    $form_title = $group->label();
    if ($parameter == 'group_node:indicator_definition') {
      $form_title .= ": Create Indicator Definition";
    }
    if ($parameter == 'group_node:obligation') {
      $form_title .= ": Create Reporting Obligation";
    }
    if ($parameter == 'group_node:target') {
      $form_title .= ": Create New Target";
    }
    return $form_title;
  }

  /**
   * Group home page build.
   *
   * @param \Drupal\group\Entity\Group $group
   *   The group Object.
   *
   * @return array
   *   Build array.
   */
  public function buildGroupHomePage(Group $group) {
    $build = [];
    $toolbarProps[] = [
      'class' => 'irt-toolbar-link1 btn btn-primary',
      'title' => 'Produce Indicator State Sheet',
      'desc' => 'I would like to view/export a state sheet for a single indicator.',
      'text class' => 'irt-toolbar-text1',
      'text markup' =>
      "<p>You have selected 'Produce Indicator State Sheet'</p>"
      . "<p>Based on existing indicator states, this process will allow you to select an existing reporting obligation, verify the date the data is valid for and export the indicator"
      . "states as an editable document.</p>"
      . "<div  class = 'col text-center' ><a href=\"/irt/{$group->id()}/select_state_sheet\" class = \"btn btn-primary\">Produce Indicator State Sheet</a></div>",
    ];
    $toolbarProps[] = [
      'class' => 'irt-toolbar-link2 btn btn-primary',
      'title' => 'Produce Report',
      'desc' => 'I would like to view/export a report using multiple existing indicator states.',
      'text class' => 'irt-toolbar-text2',
      'text markup' => "<p>You have selected 'Produce Report'</p>"
      . "<p>Based on existing indicator states, this process will allow you to select an existing reporting obligation, verify the date the data is valid for and export the collection"
      . "indicator states as an editable document.</p>"
      . "<div  class = 'col text-center' > <a href=\"/irt/{$group->id()}/select_report\" class = \"btn btn-primary\">Produce Report</a></div>",
    ];
    $toolbarProps[] = [
      'class' => 'irt-toolbar-link3 btn btn-primary',
      'title' => 'Update Indicator States',
      'desc' => 'I would like to update the state of one or more existing indicators.',
      'text class' => 'irt-toolbar-text3',
      'text markup' => "<p>You have selected 'Update Indicator State(s)'</p>"
      . "<p>Using existing indicator definitions, this tool will allow you to create or update the actual condition of an indicator. For example, using the indicator definition for"
      . "protected areas, it will allow you to report on the area and status for protected areas in the country.</p>"
      . "<div  class = 'col text-center' ><a href=\"/irt/{$group->id()}/state/step-1\" class = \"btn btn-primary\">Update Indicator State(s)</a> </div>",
    ];
    $toolbarProps[] = [
      'class' => 'irt-toolbar-link4 btn btn-primary',
      'title' => 'Manage Reporting Obligations and Indicator Definitions',
      'desc' => 'I would like to add/edit reporting obligations and indicator definitions.',
      'text class' => 'irt-toolbar-text4',
      'text markup' => "<p>You have selected 'Manage Reporting Obligations and Indicator Definitions'</p>"
      . "<p>This tool allows you to add new reporting obligations, like a quarterly ministerial report or a report to an MEA, modify an existing obligation including the targets within "
      . "the obligation, such as the Aichi targets under CBD, and add or create new indicator definitions.</p>"
      . "<div  class = 'col text-center' > <a href=\"/irt/{$group->id()}/select_obligation\" class = \"btn btn-primary\">Manage Reporting Obligations and Indicator Definitions</a></div>",
    ];
    $toolbarProps[] = [
      'class' => 'irt-toolbar-link5 btn btn-primary',
      'title' => 'Indicator Definitions',
      'desc' => 'I would like to browse/add/edit indicator definitions.',
      'text class' => 'irt-toolbar-text5',
      'text markup' => "<p>You have selected 'Indicator Definitions'.</p>"
      . "<p>This tool allows you to browse existing indicator definitions and choose which to use. "
      . "You can also create new indicator definitions and update indicator definitions created by your own group.</p>"
      . "<br><div  class = 'col text-center' > <a href=\"/irt/{$group->id()}/definitions\" class = \"btn btn-primary\">Indicator Definitions</a></div>",
    ];

    $makrup = "<div  class = 'row main-page-row'>";
    foreach ($toolbarProps as $prop) {
      $makrup .= '<div class ="col-sm-3 col-2dot4"> <div class = main-page-link><div class="' . $prop['class']
        . '" title="' . $prop['desc']
        . '">' . $prop['title'] . '</div></div><br>' . $prop['desc'] . '</div>';
    }
    $makrup .= "</div>";

    foreach ($toolbarProps as $prop) {
      $makrup .= "<div class=\"{$prop['text class']} row \">";
      $makrup .= $prop['text markup'];
      $makrup .= "</div>";
    }

    $build['group_view']['#markup'] = new FormattableMarkup($makrup, []);
    // Attaching JS/CSS.
    $build['#attached']['library'][] = 'irt/irt';

    // The operations available in this block vary per the current user's group
    // permissions. It obviously also varies per group, but we cannot know for
    // sure how we got that group as it is up to the context provider to
    // implement that. This block will then inherit the appropriate cacheable
    // metadata from the context, as set by the context provider.
    $cacheable_metadata = new CacheableMetadata();
    $cacheable_metadata->setCacheContexts(['user.group_permissions']);

    return $build;
  }

  /**
   * Producting a Report Step 2.
   *
   * @param \Drupal\group\Entity\Group $group
   *   The group Object.
   * @param \Drupal\node\Entity\Node $node
   *   The Obligation node Object.
   *
   * @return array
   *   Build array.
   */
  public function productingReportStep2(Group $group, Node $node) {
    $logger = $this->getLogger(__FUNCTION__);
    $account = User::load(\Drupal::currentUser()->id());
    if ($group->getMember($account)) {
      if ($node->bundle() != 'obligation') {
        $logger->warning("Should be an obligation node, got [{$node->bundle()}]!");
        throw new NotFoundHttpException();
      }

      $build = [];
      $markup = '<br><div class="irt-inline-links  row">';
      $markup .= "<div class=\"irt-inline-links-1 col-sm-6\">";
      $markup .= "<a href=\"/irt/{$group->id()}/obligation/{$node->id()}/export\" class=\"btn btn-outline-primary\">Export Editable Document</a>";
      $markup .= "<p> Indicator states are accurate and updated.</p></div> ";
      $markup .= "<div class=\"irt-inline-links-2 col-sm-6\">";
      $markup .= "<a href=\"/irt/{$group->id()}/state/step-1\" class=\"btn btn-outline-primary\">Goto Update Indicator State(s)</a>";
      $markup .= "<p> Some indicator states need updating.</p></div>";
      $markup .= "</div> <br>";

      $build['right'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['irt-node-obligation-col2'],
          'style' => [
            'display: inline-block;',
            'vertical-align: top;',
          ],
        ],
        '0' => ['#markup' => new FormattableMarkup($markup, [])],
        '1' => [
          '#markup' =>
          new FormattableMarkup("<h2>Reporting Obligation: {$node->label()}</h2>", []),
        ],
        '2' => $node->body->view(),
        '3' => views_embed_view('indicator_states_per_obligation', 'block_1'),
      ];

      // Setting up cache.
      $cacheable_metadata = new CacheableMetadata();
      $cacheable_metadata->setCacheContexts(['user.group_permissions']);

      return $build;
    }
    else {
      $logger->warning("Access Denied!");
      throw new HttpException('401', 'Unauthorized access.');
    }
  }

  /**
   * Step 2: Add new indicator state.
   *
   * @param \Drupal\group\Entity\Group $group
   *   The group ID.
   * @param \Drupal\node\Entity\Node $node
   *   Definition state node.
   *
   * @return array
   *   Build array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function createIndicatorStateNode(Group $group, Node $node) {
    $logger = $this->getLogger(__FUNCTION__);
    $account = User::load(\Drupal::currentUser()->id());
    $indicator_state = route_validation_create_indicator_state();
    if ($indicator_state == TRUE) {
      return $this->messenger()->addError('Indicator state for this indicator definition already exist for ' . $group->label() .
        ". Please go on step 1 to update ");
    }
    elseif ($group->getMember($account)) {
      if ($node->bundle() != 'indicator_definition') {
        $logger->warning("Should be an indicator_definition node, got [{$node->bundle()}]!");
        throw new NotFoundHttpException();
      }
      $build = [];
      // View the indicator definition first.
      $render_controller = \Drupal::entityTypeManager()
        ->getViewBuilder($node->getEntityTypeId());
      $indicator_defintion_view = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['irt-steps-node-view col-sm-5'],
          'title' => "Indicator Definition",
        ],
        '0' => ['#markup' => new FormattableMarkup('<h3><b>Indicator Definition</b></h3>', [])],
        '1' => $render_controller->view($node, 'full'),
      ];
      // $indicatorState is null, then create a new node.
      /** @var \Drupal\node\Entity\NodeType $nodeType */
      $nodeType = \Drupal::entityTypeManager()
        ->getStorage('node_type')->load('indicator_state');
      $indicatorState = $this->entityTypeManager()->getStorage('node')->create([
        'type' => $nodeType->id(),
      ]);

      // @see function group_form_alter().
      $extra = $this->forceGroupContentEnabling($group, "indicator_state");

      // Creating a new state has to be on step_2.
      $form = $this->entityFormBuilder()
        ->getForm($indicatorState, 'step_2', $extra);
      $indicator_state_create = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['irt-steps-node-view col-sm-7'],
          'title' => "Indicator Definition",
        ],
        '0' => ['#markup' => new FormattableMarkup('<h3><b>Indicator State</b></h3>', [])],
        '1' => $form,
      ];
      $build['create_node_page'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['row'],
        ],
        'indicator_definition_view' => $indicator_defintion_view,
        'indicator_state_view' => $indicator_state_create,

      ];

      // Setting up cache.
      $cacheable_metadata = new CacheableMetadata();
      $cacheable_metadata->setCacheContexts(['user.group_permissions']);

      return $build;
    }
    else {
      $logger->warning("Access Denied!");
      throw new HttpException('401', 'Unauthorized access.');
    }
  }

  /**
   * Return an array to be used for formState to force enabling GroupContent.
   *
   * @param \Drupal\group\Entity\Group $group
   *   The group object.
   * @param string $bundle
   *   The bundle.
   *
   * @return array
   *   Extras used for formState.
   */
  private function forceGroupContentEnabling(Group $group, $bundle) {
    $extra = [];
    $extra['group_wizard_id'] = 'group_entity';
    $extra['group_wizard'] = FALSE;
    $extra['group'] = $group;
    $extra['group_content_enabler'] = "group_node:$bundle";
    return $extra;
  }

  /**
   * The rest of the steps: Edit indicator state.
   *
   * If $indicatorState object is null, then create a new node, else edit the
   * node.
   *
   * @param \Drupal\group\Entity\Group $group
   *   The group ID.
   * @param \Drupal\node\Entity\Node $node
   *   Indicator State node.
   *
   * @return array
   *   Build array.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function updateIndicatorStateSteps(Group $group, Node $node) {
    $logger = $this->getLogger(__FUNCTION__);
    $account = User::load(\Drupal::currentUser()->id());
    if ($group->getMember($account)) {
      if ($node->bundle() != 'indicator_state') {
        $logger->warning("Should be an indicator_state node, got [{$node->bundle()}]!");
        throw new NotFoundHttpException();
      }

      $build = [];
      // Get internal path.
      $current_path = \Drupal::service('path.current')
        ->getPath();
      $pathArgs = explode('/', $current_path);
      if (!isset($pathArgs[4]) || empty($step = $pathArgs[4])) {
        $logger->warning("Invalid request. missing pathArgs[4].");
        throw new NotFoundHttpException();
      }
      // Match the machine name of form display names.
      $step = str_replace('-', '_', $step);
      // Get indicator definition.
      $refItems = $node->get("field_state_indicator_definition");
      /** @var \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem $refItem */
      $refItem = $refItems->first();
      $indicatorDefinition = $refItem->get('entity')->getTarget()->getValue();

      // View the indicator definition first.
      $render_controller = \Drupal::entityTypeManager()
        ->getViewBuilder($indicatorDefinition->getEntityTypeId());
      $indicator_defintion_view = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['irt-steps-node-view col-sm-5'],
          'title' => "Indicator Definition",
        ],
        '0' => ['#markup' => new FormattableMarkup('<h3><b>Indicator Definition</b></h3>', [])],
        '1' => $render_controller->view($indicatorDefinition, 'full'),
      ];

      // Edit node.
      // @see function group_form_alter().
      if ($node->isNew()) {
        // Only add group content on creation. After that it is already in a
        // group.
        $extra = $this->forceGroupContentEnabling($group, "indicator_state");
        $form = $this->entityFormBuilder()
          ->getForm($node, $step, $extra);
      }
      else {
        $form = $this->entityFormBuilder()
          ->getForm($node, $step);
      }
      $indicator_state_view = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['irt-steps-node-view col-sm-7'],
          'title' => "Indicator Definition",
        ],
        '0' => ['#markup' => new FormattableMarkup('<h3><b>Indicator State</b></h3>', [])],
        '1' => $form,
      ];
      $build['update_node_page'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['row'],
        ],
        'indicator_definition_view' => $indicator_defintion_view,
        'indicator_state_view' => $indicator_state_view,

      ];
      // Setting up cache.
      $cacheable_metadata = new CacheableMetadata();
      $cacheable_metadata->setCacheContexts(['user.group_permissions']);

      return $build;
    }
    else {
      $logger->warning("Access Denied!");
      throw new HttpException('401', 'Unauthorized access.');
    }
  }

  /**
   * Save and Review page got indicator state create/update.
   *
   * @param \Drupal\group\Entity\Group $group
   *   The group ID.
   * @param \Drupal\node\Entity\Node $node
   *   Indicator State node.
   *
   * @return array
   *   Build array.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function viewIndicatorStateNode(Group $group, Node $node) {
    $logger = $this->getLogger(__FUNCTION__);
    $account = User::load(\Drupal::currentUser()->id());
    if ($group->getMember($account)) {
      if ($node->bundle() != 'indicator_state') {
        $logger->warning("Should be an indicator_state node, got [{$node->bundle()}]!");
        throw new NotFoundHttpException();
      }

      $build = [];
      // Get internal path.
      $current_path = \Drupal::service('path.current')
        ->getPath();
      $pathArgs = explode('/', $current_path);
      if (!isset($pathArgs[4]) || empty($pathArgs[4])) {
        $logger->warning("Invalid request. missing pathArgs[4].");
        throw new NotFoundHttpException();
      }
      // Get indicator definition.
      $refItems = $node->get("field_state_indicator_definition");
      /** @var \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem $refItem */
      $refItem = $refItems->first();
      $indicatorDefinition = $refItem->get('entity')->getTarget()->getValue();

      // View the indicator definition first.
      $render_controller = \Drupal::entityTypeManager()
        ->getViewBuilder($indicatorDefinition->getEntityTypeId());

      $indicator_defintion_view = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['irt-steps-node-view col-sm-5'],
          'title' => "Indicator Definition",
        ],
        '0' => ['#markup' => new FormattableMarkup('<h3><b>Indicator Definition</b></h3>', [])],
        '1' => $render_controller->view($indicatorDefinition, 'full'),
      ];

      // View the indicator state.
      $render_controller = \Drupal::entityTypeManager()
        ->getViewBuilder($node->getEntityTypeId());
      $update_link = "<div class ='row'><div class ='col-sm'><a href=\"/irt/{$group->id()}/state/step-2/{$node->id()}\">Update</a></div>";
      $export_to_word_link = "<div class='col-sm'><a href=\"/irt/{$group->id()}/state/{$node->id()}/export\">Export to Word</a></div> </div>";
      $indicator_state_view = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['irt-steps-node-view col-sm-7'],
        ],
        '0' => ['#markup' => new FormattableMarkup('<h3><b>Indicator State</b></h3>', [])],
        '1' => ['#markup' => new FormattableMarkup($update_link, [])],
        '2' => ['#markup' => new FormattableMarkup($export_to_word_link, [])],
        '3' => $render_controller->view($node, 'full'),
      ];

      if (!empty($node->obligations_and_targets)) {
        $obligation_and_targets = $node->obligations_and_targets->value;
      }
      $obligation_and_targets_heading = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['irt-steps-node-view col-sm-5'],
        ],
        '0' => ['#markup' => new FormattableMarkup('<h5><b>Related Reporting Obligations / Targets:</b></h5>', [])],
      ];
      $obligation_and_targets_view = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['irt-steps-node-view col-sm-7'],
        ],
        '0' => ['#markup' => $obligation_and_targets],
      ];
      $build['review_page'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['row'],
        ],
        'obligation_and_targets_heading' => $obligation_and_targets_heading,
        '$obligation_and_targets_view' => $obligation_and_targets_view,
        'indicator_definition_view' => $indicator_defintion_view,
        'indicator_state_view' => $indicator_state_view,

      ];

      // Setting up cache.
      $cacheable_metadata = new CacheableMetadata();
      $cacheable_metadata->setCacheContexts(['user.group_permissions']);

      return $build;
    }
    else {
      $logger->warning("Access Denied!");
      throw new HttpException('401', 'Unauthorized access.');
    }
  }

  /**
   * Get the title for review page.
   *
   * @param \Drupal\group\Entity\Group $group
   *   The group ID.
   * @param \Drupal\node\Entity\Node $node
   *   Indicator State node.
   *
   * @return array
   *   Title of node indicator_state.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getTitle(Group $group, Node $node) {
    if (!empty($node)) {
      return $node->getTitle();
    }
  }

}

/**
 * Validation on route of create indicator state.
 *
 * @return bool
 *   True if indicator state exist.
 *
 * @throws \Drupal\Core\TypedData\Exception\MissingDataException
 */
function route_validation_create_indicator_state() {
  $indicator_definition_id = \Drupal::routeMatch()->getRawParameter('node');
  $group_id = \Drupal::routeMatch()->getRawParameter('group');
  if (!empty($indicator_definition_id)) {
    $indicator_def_node = Node::load($indicator_definition_id);
    // Load group.
    $group = Group::load($group_id);
    $plugin_id = 'group_node:indicator_state';
    // Get group content type id.
    $group_content_type_id = $group->getGroupType()->getContentPlugin($plugin_id)->getContentTypeConfigId();
    $indicator_state_group_content_ids = \Drupal::entityQuery('group_content')
      ->condition('type', $group_content_type_id)
      ->condition('gid', $group_id)
      ->condition('entity_id.entity.field_state_indicator_definition.entity', $indicator_def_node->id())
      ->range(0, 1)
      ->accessCheck(TRUE)
      ->execute();
    $indicator_state_group_content_id = end($indicator_state_group_content_ids);
    if ($indicator_state_group_content_id) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
}
