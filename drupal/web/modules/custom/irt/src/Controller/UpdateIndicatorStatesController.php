<?php

namespace Drupal\irt\Controller;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\group\Entity\GroupInterface;
use Drupal\irt\UtilityTrait;

/**
 * Returns responses for Export document of state per obligation.
 */
class UpdateIndicatorStatesController extends ControllerBase {

  use UtilityTrait;

  /**
   * Produce Indicator state in groyp.
   */
  public function viewIndicatorStates() {
    if (\Drupal::currentUser()->isAnonymous()) {
      return [];
    }
    // Get group id from the route.
    $route_group_id = \Drupal::routeMatch()->getRawParameter('group');

    // Entity query to get all the indicator definitions.
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'indicator_definition')
      ->condition('status', 1)
      ->accessCheck(TRUE);
    $nids = $query->execute();
    $node_storage = \Drupal::entityTypeManager()->getStorage('node');
    $indicator_def_nodes = $node_storage->loadMultiple($nids);
    $rows = [];
    $headers = [
      'indicator_definition' => 'Indicator Definition',
      'reporting_obligations_and_targets' => 'Reporting Obligations & Targets ',
      'indicator_state' => 'Indicator State',
    ];

    foreach ($indicator_def_nodes as $indicator_def_node) {
      $indicator_def_group = $this->getGroupByEntity($indicator_def_node);
      $sprep_group_id = $this->getSprepGroupId();
      if ($indicator_def_group->id() == $route_group_id || $indicator_def_group->id() == $sprep_group_id) {
        $rows[$indicator_def_node->id()]['indicator_definition'] = new FormattableMarkup("<a href=\"@nodeUrl\" target=\"_blank\">@title</a>",
              [
                '@title' => $indicator_def_node->title->value,
                '@nodeUrl' => "/node/" . $indicator_def_node->id(),
              ]);
        $render_controller = \Drupal::entityTypeManager()
          ->getViewBuilder($indicator_def_node->getEntityTypeId());
        $output = $render_controller->view($indicator_def_node, 'obligations_and_targets');
        $rows[$indicator_def_node->id()]['reporting_obligations_and_targets'] = \Drupal::service('renderer')->render($output);
        // Load group.
        $group = Group::load($route_group_id);
        $plugin_id = 'group_node:indicator_state';
        // Get group content type id.
        $group_content_type_id = $group->getGroupType()->getContentPlugin($plugin_id)->getContentTypeConfigId();

        // Get all the indicator states group content items related to
        // indicator definition for this group.
        $indicator_state_group_content_ids = \Drupal::entityQuery('group_content')
          ->condition('type', $group_content_type_id)
          ->condition('gid', $route_group_id)
          ->condition('entity_id.entity.field_state_indicator_definition.entity', $indicator_def_node->id())
          ->range(0, 1)
          ->accessCheck(TRUE)
          ->execute();
        $indicator_state_group_content_id = end($indicator_state_group_content_ids);
        // Check if indicator state(s) exits.
        if (!empty($indicator_state_group_content_id)) {

          $group_content = GroupContent::load($indicator_state_group_content_id);
          $indicator_state_entity = $group_content->getEntity();
          if (!empty($indicator_state_entity)) {
            $output = [
              '#theme' => 'symbols',
              '#image' => 'Image missing-value not selected',
            ];
            // Show assessment icon if available.
            foreach ($indicator_state_entity->field_indicator_assessment as $i => $value) {
              $output = $value->view(['type' => 'assessment_symbols_field_formatter']);
            }
            $rows[$indicator_def_node->id()]['indicator_state'] = new FormattableMarkup(\Drupal::service('renderer')->render($output)
                . "<a href=\"@url\" class=\"btn btn-primary\">@title</a>",
                [
                  '@title' => 'Update Indicator State',
                  '@url' => '/irt/' . $route_group_id . '/state/step-2/' . $indicator_state_entity->id(),
                ]);
          }
        }
        // If indicator state doesn't exist for group then show create link.
        else {
          $rows[$indicator_def_node->id()]['indicator_state'] = new FormattableMarkup("<a href=\"@url\" class=\"btn btn-primary\">@title</a>",
              [
                '@title' => 'Create Indicator State',
                '@url' => "/irt/" . $route_group_id . "/state/step-2/create/" . $indicator_def_node->id(),
              ]);
        }
      }
    }

    $table = [
      '#type' => 'table',
      '#attributes' => [
        'class' => ['table'],
      ],
      '#header' => $headers,
      '#rows' => $rows,
      '#sticky' => TRUE,
      '#cache' => ['max-age' => 0],
    ];
    return $table;

  }

  /**
   * Get the title for review page.
   *
   * @return string
   *   Title of node indicator_state.
   */
  public function getTitle() {
    return 'Update Indicator State Step 1: Select indicator definition';
  }

  /**
   * Get the group of the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return object
   *   Group of the entity.
   */
  public function getGroupByEntity(EntityInterface $entity) {
    $group = FALSE;
    if ($entity instanceof GroupInterface) {
      return $entity;
    }
    $preferred_id = 'countries-group_' . $entity->getEntityTypeId() . '-' . $entity->bundle();
    // Group module uses a hashed ID if the readable ID would exceed the
    // maximum length.
    if (strlen($preferred_id) > EntityTypeInterface::BUNDLE_MAX_LENGTH) {
      $hashed_id = 'group_content_type_' . md5($preferred_id);
      $preferred_id = substr($hashed_id, 0, EntityTypeInterface::BUNDLE_MAX_LENGTH);
    }
    // Load all the group content for this entity.
    /** @var \Drupal\group\Entity\GroupContent $group_content */
    $group_content = \Drupal::entityTypeManager()->getStorage('group_content')
      ->loadByProperties([
        'type' => $preferred_id,
        'entity_id' => $entity->id(),
      ]);
    // Assuming that the content can be related only to 1 group.
    $group_content = reset($group_content);
    if (!empty($group_content)) {
      $group = $group_content->getGroup();
    }
    return $group;
  }

  /**
   * Get the group  id of sprep group.
   *
   * @return string
   *   Group id of SPREP group.
   */
  public function getSprepGroupId() {
    $sprep_group_id = \Drupal::database()->query('SELECT g.id FROM {groups_field_data} g WHERE g.label = :label', [':label' => "SPREP"])->fetchAssoc();
    if ($sprep_group_id) {
      return $sprep_group_id['id'];
    }
  }

}
