<?php

namespace Drupal\irt\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\irt\UtilityTrait;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Returns responses for Export document of state per obligation.
 */
class IndicatorDefinitionController extends ControllerBase {

  use UtilityTrait;

  /**
   * Produce Indicator state in groyp.
   */
  public function createIndicatorDefinition() {
    if (\Drupal::currentUser()->isAnonymous()) {
      return [];
    }
    $user = User::load(\Drupal::currentUser()->id());
    $groups = [];
    $grp_membership_service = \Drupal::service('group.membership_loader');
    $grps = $grp_membership_service->loadByUser($user);
    // Get all the groups of current user.
    foreach ($grps as $grp) {
      $groups[] = $grp->getGroup();
    }
    $number_of_groups = count($groups);
    if ($number_of_groups > 1) {
      $rows = [];
      $headers = [
        'group' => 'Group',
        'link' => 'Link',
      ];
      // Create two-step form if user have more than one group.
      foreach ($groups as $group) {
        $url = Url::fromRoute('entity.group_content.create_form', [
          'group' => $group->id(),
          'plugin_id' => 'group_node:indicator_definition',
        ], ['absolute' => TRUE]);
        $link = Link::fromTextAndUrl('Create Indicator Definition for ' . $group->label(), $url);
        $rows[$group->id()]['group'] = $group->label();
        $rows[$group->id()]['link'] = $link->toString();

      }
      $table = [
        '#type' => 'table',
        '#header' => $headers,
        '#rows' => $rows,
        '#sticky' => TRUE,
      ];
      return $table;
    }
    // Create redirect if user user only have one group.
    else {
      foreach ($groups as $group) {
        $group_id = $group->id();
      }

      $url = Url::fromRoute('entity.group_content.create_form', [
        'group' => $group_id,
        'plugin_id' => 'group_node:indicator_definition',
      ], ['absolute' => TRUE]);

      $response = new RedirectResponse($url->toString());
      $response->send();
    }

  }

}
