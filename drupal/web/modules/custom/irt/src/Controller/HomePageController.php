<?php

namespace Drupal\irt\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\irt\UtilityTrait;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Returns redirects for user based on permissions.
 */
class HomePageController extends ControllerBase {

  use UtilityTrait;

  /**
   * Redirect for user based on group.
   */
  public function homePageRedirect() {
    if (\Drupal::currentUser()->isAnonymous()) {
      // Redirect anonymous users to the login page.
      $response = new RedirectResponse('/user/login');
      return $response;
    }
    else {
      $user = User::load(\Drupal::currentUser()->id());
      $groups = [];
      $grp_membership_service = \Drupal::service('group.membership_loader');
      $grps = $grp_membership_service->loadByUser($user);

      // Get all the groups of current user.
      foreach ($grps as $grp) {
        $groups[] = $grp->getGroup();
      }

      // Create redirect if user only has one group.
      $group_id = NULL;
      foreach ($groups as $group) {
        $group_id = $group->id();
      }

      // Generate URL for the group page.
      $url = Url::fromRoute('view.my_groups.page_1', [
        'group' => $group_id,
      ], ['absolute' => TRUE]);

      // Log the URL for debugging purposes.
      error_log($url->toString());

      // Redirect to the group page.
      $response = new RedirectResponse($url->toString());
      return $response;
    }
  }

}
