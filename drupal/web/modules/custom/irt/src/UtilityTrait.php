<?php

namespace Drupal\irt;

/**
 * Trait UtilityTrait is the utility trait.
 */
trait UtilityTrait {

  /**
   * Helper function to provide a classId::functionId.
   *
   * @return string
   *   Returns a combination of className:functionName.
   */
  public function getCallerId($function) {
    return __CLASS__ . ":" . $function;
  }

  /**
   * Load node by property.
   *
   * @param array $properties
   *   The props of a node.
   *
   * @return mixed|null
   *   Returns the node object or null.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadNodeByProperties(array $properties) {
    $existing = current(\Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadByProperties($properties));
    return $existing;
  }

  /**
   * Helper function to log kint.html to the Drupal root.
   *
   * @param mixed $mixed
   *   Anything.
   * @param string $destination
   *   The file destination, default is /tmp/ .
   */
  public function kint($mixed, $destination = '') {
    \Drupal::service('eopts.commands')->kint($mixed, $destination);
  }

}
