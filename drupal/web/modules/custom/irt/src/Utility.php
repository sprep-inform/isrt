<?php

namespace Drupal\irt;

/**
 * Class Utility.
 *
 * Traits can't be used in .module files, so I created this class (I think).
 *
 * @package Drupal\irt
 */
class Utility {

  use UtilityTrait;

}
