<?php

namespace Drupal\irt\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber subscribes to routes.
 *
 * @package Drupal\irt\Routing
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    /** @var \Symfony\Component\Routing\Route $route */
    // Route the group view page to group/{group}/timeline.
    if ($route = $collection->get('entity.group.canonical')) {
      $route->setPath('/group/{group}/home');
      $defaults = $route->getDefaults();
      // $defaults['_entity_view'] = 'group.home';.
      $defaults['_title_callback'] = '\Drupal\irt\Controller\IrtController::groupHomeTitle';
      $defaults['_controller'] = '\Drupal\irt\Controller\IrtController::buildGroupHomePage';
      $route->setDefaults($defaults);
    }
    if ($route = $collection->get('entity.group_content.create_form')) {
      $defaults = $route->getDefaults();
      $defaults['_title_callback'] = '\Drupal\irt\Controller\IrtController::groupNodeFormTitle';
      $route->setDefaults($defaults);
    }
    if ($route = $collection->get('entity.node.edit_form')) {
      // \Drupal::service('eopts.commands')->kint($route);
    }

  }

}
