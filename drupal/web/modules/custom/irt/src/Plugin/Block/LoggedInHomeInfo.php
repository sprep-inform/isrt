<?php

namespace Drupal\irt\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides a 'LoggedInHomeInfo' block.
 *
 * @Block(
 *  id = "logged_in_home_info",
 *  admin_label = @Translation("Logged In home info"),
 * )
 */
class LoggedInHomeInfo extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $markup = '<br><div class = "row main-page-row">';
    $markup .= '<div class = "col-sm-12">';
    $markup .= '<h2 class = "text-center"><b>How does indicator reporting work?</b></h2>';
    $markup .= '<p> <b>Reporting Obligations:</b> SDG Reporting, State of Environment, Aichi Target, National Development Goals etc reporting obligations.</p>';
    $markup .= '<p>A reporting obligation represents a report that needs to be prepared in a format that uses indicators to demonstrate status/progress according to a broadly defined target / goal. This is a typical method of reporting against nationally defined and internationally defined development and environmental targets.</p>';
    $markup .= '<p>A reporting obligation is a collection of indicators grouped by target.</p>';
    $markup .= '</div></div>';

    $markup .= '<br><div class = "row main-page-row-grey">';
    $markup .= '<div class = "col-sm-12">';
    $markup .= '<h1 class = "text-center"><b>Example structure of an indicator based report.</b></h1>';
    $markup .= '<p> <b>Reporting Obligation: State of Environment Report</b></p>';
    $markup .= '<p><b>Target:</b> Climate Adaptation and Resilience to Climate Change Impacts</p>';
    $markup .= '<p><b>&nbsp;&nbsp;&nbsp;&nbsp;Indicator:</b> Climate Adaptation Actions to improve water, food and health security</p>';
    $markup .= '<p><b>&nbsp;&nbsp;&nbsp;&nbsp;Indicator:</b>  Integrated climate change measures into national policies, strategies, infrastructure and buildings</p>';
    $markup .= '<p><b>Target:</b> Good Stream Water Quality</p>';
    $markup .= '<p><b>&nbsp;&nbsp;&nbsp;&nbsp;Indicator:</b> Levels of bacteria and nutrients found in fresh water streams</p>';
    $markup .= '<p><b>Target:</b> Healthy Marine Environment</p>';
    $markup .= '<p><b>&nbsp;&nbsp;&nbsp;&nbsp;Indicator:</b> Live coral cover</p>';
    $markup .= '<p><b>&nbsp;&nbsp;&nbsp;&nbsp;Indicator:</b> Reef fish and urchin density and biomass</p>';
    $markup .= '</div></div>';

    $markup .= '<br><div class = "row main-page-row">';
    $markup .= '<div class = "col-sm-6">';
    $markup .= '<h2 class = "text-center"><b>Indicator Definition - "how to measure"</b></h2>';
    $markup .= '<p> Indicator definitions are the specific descriptions of how an indicator is defined, the methodology to measure it, units of measurements etc.</p>';
    $markup .= '<p> A well defined indicator should allow for comparable measurements to be collectedover time to identify trends.</p>';
    $markup .= '</div>';
    $markup .= '<div class = "col-sm-6">';
    $markup .= '<h2 class = "text-center"><b>Indicator State - "the measurements"</b></h2>';
    $markup .= '<p> An indicator state records the measurements and trends at a given point in time according to the instructions defined in the related indicator definition.</p>';
    $markup .= '</div>';
    $markup .= '</div>';

    $markup .= '<br><div class = "row ">';
    $markup .= '<div class = "col-sm-6 layout-sidebar-first">';
    $markup .= '<h1 class = "text-center"><b>Example Indicator Definition</b></h1>';
    $markup .= '<p> <b>Name:</b></p>';
    $markup .= '<p>Area of terrestrial protected area</p>';
    $markup .= '<p> <b>Definition:</b></p>';
    $markup .= '<p>Protected areas are at established to protect biodiversity and ecosystem values from resource extraction and unsustainable harvesting.</p>';
    $markup .= '<p> <b>Desired Outcome:</b></p>';
    $markup .= '<p>Increase in protected areas or; all terrestrial ecosystems are adequately represented in the protected areas network or; >17% of land area is protected (Aichi target 11)</p>';
    $markup .= '<p> <b>Measurement or Calculation:</b></p>';
    $markup .= '<p>(total protected area/total land area)*100</p>';
    $markup .= '<p> <b>Unit of Measurement:</b></p>';
    $markup .= '<p>ha or km2</p>';
    $markup .= '<p> <b>Rationale and Assumptions:</b></p>';
    $markup .= '<p>All protected areas accurately mapped</p>';
    $markup .= '<p> <b>Preferred Data Sources:</b></p>';
    $markup .= '<p>National environment agencies, WDPA, BIOPAMA Project portal</p>';
    $markup .= '</div>';

    $markup .= '<div class = "col-sm-6 layout-sidebar-first">';
    $markup .= '<h1 class = "text-center"><b>Example Indicator State</b></h1>';
    $markup .= '<p> <b>Key Findings:</b></p>';
    $markup .= '<p>Cook Islands has 14 terrestrial protected areas which are six per cent
                  of the total land mass. The PAs include private nature reserves, conservation
                  areas, entire island and motu PAs, a wildlife sanctuary, National Parks and
                Reserves, and community managed areas. However most PAs are not covered by legislation. KBA and IBA provide a guide for conservationa</p>';
    $markup .= '<p> <b>Status:</b></p>';
    $image_schema = 'public://good_improving_low.png';
    $style = ImageStyle::load('medium');
    $url = $style->buildUrl($image_schema);
    $image_link = \Drupal::service('file_url_generator')->generateAbsoluteString($url);
    $markup .= '<p> <img src = "' . $image_link . '"></p>';
    $markup .= '<p> <b>Impact:</b></p>';
    $markup .= '<p>Protected areas allow ecosystems to recover relatively free from human
                  exploitation,directly prevent ongoing loss of biodiversity, replenish species
                  populations and encourage better stewardship by landowners.
                  Protectionofnaturalforests, particularly the Makatea forests, hasresultedinmore
                  protection of the habitats for endemic and native bat and bird species.
                  Rarotonga’s Takitumu Conservation Area (TCA) is a good example of a
                  protected area with focused management regimes for the preservation
                  of endangered and vulnerable species. The TCA was established in 1996
                  under a landowner committee to protect endangered native birds and their
                  habitat. In 1989 the endemic K?ker?ri (Rarotongan flycatcher, Pomarea dimidiata)
                  was considered one of the ten rarest bird species in the world, with only 29 birds
                  in the TCA. With focused protection efforts through the TCA in 1996,
                  the population recovered to 255 individuals in 2001, 291 individuals in 2006,
                  and 330 individuals in 2009 on Rarotonga. While the 32.5 ha Highland Paradise
                  is a popular nature reserve with tourists and locals, large numbers of visitors can
                  have mixed and sometimes negative impacts upon the very environments that
                  the reserve works to protect. Increased tourism can result in the clearing and
                  removal of native plants and, as a result, the reserve will need to use more
                  protective measures for important native plants as it welcomes
                  more visitors. Highland Paradise is an example to other PAs about the
                  unintended consequences with PA designation and the rise in visitor numbers,
                  especially with growing tourism. Six per cent of the Cook Islands land mass is
                  protected. A further 11% would need to be declared as PA to fulfil the goal
                  under the CBD Aichi Targets by 2020 of 17% terrestrial protected areas</p>';
    $markup .= '<p> <b>Response and Recommendations:</b></p>';
    $markup .= '<p>Cook Islands has made good progress in establishing protected areas to
                   contribute their national commitment under the Convention for Biological
                   Diversity of declaring 17% of their terrestrial areas as PA by 2020. However,
                   more declared PAs or terrestrial management plans are needed. The existing
                   five PA need to be formally mapped, which will increase the committed area
                   under the PA status. Protected area data should be entered into a database.
                   Cook Islands may consider using the Pacific Islands Protected Area Portal
                   (PIPAP). Communities should continue to be involved in the establishment
                   of protected areas and their participation could be linked to alternative
                   livelihood projects.The declaration of Marae Moana is an important
                   achievement, especially for the marine area, and with a ridge to reef
                   approach it would provide an opportunity for a holistic approach and
                   the inclusion of terrestrial management areas. A field survey was conducted
                   over three weeks in 2015 in the cloud forest on Rarotonga and the data was
                   used to develop a management and restoration plan for the cloud forest.
                   This is a good opportunity to increase active management and protect some
                   of the rarest habitat in the Cook Islands. Key Biodiversity Areas (KBA) and
                   Important Bird Areas (IBA) have been identified which provide a guide for
                   conservation. Some high priority areas include developing regulations for
                   Suwarrow, and regulations and management plans for Takuvaine. It would
                   be worth having a terrestrial PA committee made up of stakeholders from
                   relevant sectors including environment, agriculture, tourism and civil society.
                   This could be a subcommittee to one of the existing working groups such as
                   the NBSAP working group. Recommendations from the cloud forest management
                   plan and the 4th national report to the CBD in regards to protected areas should
                   be implemented. It is important to prioritise and resource PA monitoring to ensure
                   good management practices and proper record keeping takes place.</p>';
    $markup .= '<p> <b>Sources:</b></p>';
    $markup .= '<p>National Environment Service. 2011. Cook Islands 4th National Report to the Convention on Biological Biodiversity.</p>';
    $markup .= '<p> <b>Personal communication:</b></p>';
    $markup .= '<p>Takitumu Conservation Area -Ian and Papa Ed<br>House of Ariki and Koutu Nui <br>KBA & IBA ? Kelvin and Jacqui <br>NHT - Gerald</p>';
    $markup .= '<p> <b>Online source:</b></p>';
    $markup .= '<p>http://nescookislands.com/<br>www.iucn.org/about/work/programmes/gpap_home</p>';
    $markup .= '</div>';
    $markup .= '</div>';

    $build['logged_in_home_info']['#markup'] = $markup;

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    if (\Drupal::currentUser()->isAnonymous()) {
      // Anonymous user.
      \Drupal::messenger()->addError('Acess denied');
      return AccessResult::forbidden();

    }
    else {
      return AccessResult::allowed();
    }
  }

}
