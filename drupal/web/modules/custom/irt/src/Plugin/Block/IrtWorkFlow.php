<?php

namespace Drupal\irt\Plugin\Block;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\group\Context\GroupRouteContextTrait;
use Drupal\irt\UtilityTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'irt_workflow' block.
 *
 * @Block(
 *  id = "irt_workflow",
 *  admin_label = @Translation("IRT Workflow"),
 * )
 */
class IrtWorkFlow extends BlockBase implements ContainerFactoryPluginInterface {

  use GroupRouteContextTrait;

  use UtilityTrait;

  /**
   * Constructs a new IrtMainBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The plugin CurrentRouteMatch.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    CurrentRouteMatch $current_route_match,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (\Drupal::currentUser()->isAnonymous()) {
      return [];
    }
    $build = [];
    $build['irt_workflow']['#markup'] = new FormattableMarkup($this->buildLinks(), []);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), [
      'url.path',
      'url.query_args',
      'user.group_permissions',
    ]);
  }

  /**
   * Helper to build workflow links.
   */
  protected function buildLinks() {
    $params = $this->getCurrentPageParameters();
    $current_page = $params['current_page'];
    $gid = $params['gid'];
    $nid = $params['nid'];

    $links = self::getLinks();

    $makrup = " ";

    switch ($current_page) {
      case 'step-1':
        unset($links['step-2-u']);
        foreach ($links as $step => $link) {
          if ($step == 'step-1') {
            $line = $this->getLinkMarkup($link, $gid, $nid);
          }
          else {
            $line = $this->getEmptyLinkMarkup($link);
          }
          $line = $this->setActiveLink($line, $current_page, $step);
          $makrup .= $line;
        }
        break;

      case 'step-2-c':
        unset($links['step-2-u']);
        foreach ($links as $step => $link) {
          if ($step == 'step-1' || $step == 'step-2-c') {
            $line = $this->getLinkMarkup($link, $gid, $nid);
          }
          else {
            $line = $this->getEmptyLinkMarkup($link);
          }
          $line = $this->setActiveLink($line, $current_page, $step);
          $makrup .= $line;
        }
        break;

      case 'step-2-u':
      case 'step-3':
      case 'step-4':
      case 'step-5':
      case 'step-6':
      case 'step-7':
        unset($links['step-2-c']);
        foreach ($links as $step => $link) {
          $line = $this->getLinkMarkup($link, $gid, $nid);
          $line = $this->setActiveLink($line, $current_page, $step);
          $makrup .= $line;
        }
        break;
    }
    return $makrup;
  }

  /**
   * Get a proper link.
   *
   * @param array $link
   *   The link.
   * @param int $gid
   *   Group id.
   * @param int $nid
   *   Node id.
   *
   * @return mixed
   *   Return the link markup.
   */
  private function getLinkMarkup(array $link, $gid, $nid) {
    $stub = '<a href="{link}" class="{classes} step-links btn-outline-primary"> {stepLabel} </a>';
    $linkMarkup = str_replace("{link}", $link[0], $stub);
    $linkMarkup = str_replace("{stepLabel}", $link[1], $linkMarkup);
    $linkMarkup = str_replace("{group}", $gid, $linkMarkup);
    $linkMarkup = str_replace("{node}", $nid, $linkMarkup);
    return $linkMarkup;
  }

  /**
   * Get a URL for specific step.
   *
   * @param string $step
   *   Current page step.
   * @param int $gid
   *   Group id.
   * @param int $nid
   *   Node id.
   *
   * @return mixed
   *   Return a URL string.
   */
  private function getStepUrl($step, $gid, $nid) {
    $links = self::getLinks();
    $url = $links[$step][0];
    $url = str_replace("{group}", $gid, $url);
    $url = str_replace("{node}", $nid, $url);
    return $url;
  }

  /**
   * Get an empty link.
   *
   * @param array $link
   *   The link array.
   *
   * @return mixed
   *   Return the link markup.
   */
  private function getEmptyLinkMarkup(array $link) {
    $stub_nolink = '<a href="" class="{classes} step-links btn-outline-primary">{stepLabel}</a>';
    $linkMarkup = str_replace("{stepLabel}", $link[1], $stub_nolink);
    return $linkMarkup;
  }

  /**
   * Set an active class on the link.
   *
   * @param string $linkMarkup
   *   Link markup.
   * @param string $current_page
   *   Current page.
   * @param string $step
   *   Current step.
   *
   * @return mixed
   *   The link markup.
   */
  private function setActiveLink($linkMarkup, $current_page, $step) {
    if ($current_page === $step) {
      $linkMarkup = str_replace("{classes}", "active", $linkMarkup);
    }
    else {
      $linkMarkup = str_replace("{classes}", "", $linkMarkup);
    }
    return $linkMarkup;
  }

  /**
   * Gets current pages parameters [current_page,gid,nid].
   *
   * @return array
   *   Returns [current_page,gid,nid].
   */
  public function getCurrentPageParameters() {
    $nid = 0;
    $gid = 0;
    $current_page = '';

    // Get internal path.
    $current_path = \Drupal::service('path.current')
      ->getPath();
    $pathArgs = explode('/', $current_path);
    if (isset($pathArgs[2])) {
      $gid = $pathArgs[2];
    }
    if (isset($pathArgs[4])) {
      $current_page = $pathArgs[4];
    }

    // If arg[5] is create. then arg[6] is state.
    if (isset($pathArgs[5]) && !empty($pathArgs[5])
      && ($create = $pathArgs[5] === 'create')) {
      $nid = $pathArgs[6];
      $current_page = 'step-2-c';
    }
    else {
      if (isset($pathArgs[5]) && !empty($pathArgs[5])) {
        $nid = $pathArgs[5];
        if ($pathArgs[4] === 'step-2') {
          $current_page = 'step-2-u';
        }
      }
    }

    return [
      'current_page' => $current_page,
      'gid' => $gid,
      'nid' => $nid,
      'current_path' => $current_path,
      'pathArgs' => $pathArgs,
    ];
  }

  /**
   * Statically defines links for IRT workflow.
   *
   * @return array
   *   Returns the links.
   */
  public static function getLinks() {
    $links = [
      'step-1' => ['/irt/{group}/state/step-1', 'Step 1'],
      'step-2-c' => ['/irt/{group}/state/step-2/create/{node}', 'Step 2'],
      'step-2-u' => ['/irt/{group}/state/step-2/{node}', 'Step 2'],
      'step-3' => ['/irt/{group}/state/step-3/{node}', 'Step 3'],
      'step-4' => ['/irt/{group}/state/step-4/{node}', 'Step 4'],
      'step-5' => ['/irt/{group}/state/step-5/{node}', 'Step 5'],
      'step-6' => ['/irt/{group}/state/step-6/{node}', 'Step 6'],
      'step-7' => ['/irt/{group}/state/step-7/{node}', 'Step 7'],
      'step-review' => ['/irt/{group}/state/view-step/{node}', 'Review'],
    ];
    return $links;
  }

  /**
   * Gets the next step in IRT workflow.
   */
  public function getNextStep($newNid) {
    $params = $this->getCurrentPageParameters();
    $current_page = $params['current_page'];
    $gid = $params['gid'];
    $nid = $params['nid'];

    $nextStepUrl = '';
    switch ($current_page) {
      case 'step-1':
        break;

      case 'step-2-c':
        // After saving the node, get nid and redirect to step 3.
        $nextStepUrl = $this->getStepUrl('step-3', $gid, $newNid);
        break;

      case 'step-2-u':
        // Get nid and redirect to step 3.
        $nextStepUrl = $this->getStepUrl('step-3', $gid, $nid);
        break;

      case 'step-3':
        // Redirect to step 4.
        $nextStepUrl = $this->getStepUrl('step-4', $gid, $nid);
        break;

      case 'step-4':
        $nextStepUrl = $this->getStepUrl('step-5', $gid, $nid);
        break;

      case 'step-5':
        $nextStepUrl = $this->getStepUrl('step-6', $gid, $nid);
        break;

      case 'step-6':
        $nextStepUrl = $this->getStepUrl('step-7', $gid, $nid);
        break;

      case 'step-7':
        $nextStepUrl = "/irt/$gid/state/view-step/$nid";
        break;
    }
    return $nextStepUrl;
  }

}
