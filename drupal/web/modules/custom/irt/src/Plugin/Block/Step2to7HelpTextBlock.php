<?php

namespace Drupal\irt\Plugin\Block;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a 'Step2to7HelpTextBlock' block.
 *
 * @Block(
 *  id = "step2_to_7_help_text_block",
 *  admin_label = @Translation("Step2 to 7 Help Text Block"),
 * )
 */
class Step2to7HelpTextBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (\Drupal::currentUser()->isAnonymous()) {
      return [];
    }
    $build = [];
    $build['irt_step1_to_step7_help_text']['#markup'] = new FormattableMarkup($this->helpText(), []);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['my_block_settings'] = $form_state->getValue('my_block_settings');
  }

  /**
   * Helper to create help text.
   */
  protected function helpText() {
    $params = $this->getCurrentPageParameters();
    $current_page = $params['current_page'];
    $gid = $params['gid'];
    $nid = $params['nid'];
    $markup = "<p>";
    switch ($current_page) {
      case 'step-1':
        $markup .= "Select ‘create indicator state’ where state has not previously been created or ‘update indicator state’ (shown beneath image) where state already exists</p>";
        break;

      case 'step-2-c':
      case 'step-2-u':
        $markup .= "The narrative should explain the current state and impact. Include a graph and or map illustrating the indicator state (can be linked to data portal).</p>";
        break;

      case 'step-3':
        $markup .= "Describe the impact the indicator state has on local resources, people and environment.</p>";
        break;

      case 'step-4':
        $markup .= "Detail any current national actions, including policies and project development. Outline recommendations for further action.</p>";
        break;

      case 'step-5':
        $markup .= "Write a 3 to 4 sentence summary on the current status, impact and response.</p>";
        break;

      case 'step-6':
        $markup .= "Classify the indicator state based on the trend of the indicator( up, down, mixed, neutral or unknown), the status (good, fair, poor) and confidence level.</p>";
        break;

      case 'step-7':
        $markup .= "Provide references / links to published and unpublished materials. Include links to the data portal where possible or other originating sources.</p>";
        break;
    }
    return $markup;

  }

  /**
   * Gets current pages parameters [current_page,gid,nid].
   *
   * @return array
   *   Returns [current_page,gid,nid].
   */
  public function getCurrentPageParameters() {
    $nid = 0;
    $gid = 0;
    $current_page = '';

    // Get internal path.
    $current_path = \Drupal::service('path.current')
      ->getPath();
    $pathArgs = explode('/', $current_path);
    if (isset($pathArgs[2])) {
      $gid = $pathArgs[2];
    }
    if (isset($pathArgs[4])) {
      $current_page = $pathArgs[4];
    }

    // If arg[5] is create. then arg[6] is state.
    if (isset($pathArgs[5]) && !empty($pathArgs[5])
          && ($create = $pathArgs[5] === 'create')) {
      $nid = $pathArgs[6];
      $current_page = 'step-2-c';
    }
    else {
      if (isset($pathArgs[5]) && !empty($pathArgs[5])) {
        $nid = $pathArgs[5];
        if ($pathArgs[4] === 'step-2') {
          $current_page = 'step-2-u';
        }
      }
    }

    return [
      'current_page' => $current_page,
      'gid' => $gid,
      'nid' => $nid,
      'current_path' => $current_path,
      'pathArgs' => $pathArgs,
    ];
  }

  /**
   * CacheContexts per route.
   *
   * @return array
   *   Returns CacheContext.
   */
  public function getCacheContexts() {
    return ['route'];

  }

}
