<?php

namespace Drupal\irt\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a 'ReportingObligationManageFrameworkBlock' block.
 *
 * @Block(
 *  id = "reporting_obligation_manage_framework_block",
 *  admin_label = @Translation("Reporting Obligation Manage Framework Block"),
 * )
 */
class ReportingObligationManageFrameworkBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (\Drupal::currentUser()->isAnonymous()) {
      return [];
    }
    $build = [];
    $markup = t('<b>Reporting Obligation Framework</b><br>A reporting obligation framework is a collection of indicator definitions grouped by target.');
    $markup .= t('<br><br><b>Targets</b><br>  Every Reporting obligation has one or more targets, for example the Aichi targets under the Convention for Biodiversity (CBD). Every target has one or more indicators, for example Aichi target 11 sets a 17% terrestrial area target and the terrestrial protected area indicator provides a measure for this target.');
    $markup .= t('<br><br><b>Indicator Definitions</b><br>Indicator definitions are the specific descriptions of how an indicator is defined, the methodology to measure it, units of measurements etc');
    $build['reporting_obligation_manage_framework_block']['#markup'] = $markup;
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['my_block_settings'] = $form_state->getValue('my_block_settings');
  }

}
