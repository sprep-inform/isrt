<?php

namespace Drupal\irt\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a 'UpdateIndicatorStateStep1Block' block.
 *
 * @Block(
 *  id = "update_indicator_state_step1_block",
 *  admin_label = @Translation("Update Indicator State Step1 Block"),
 * )
 */
class UpdateIndicatorStateStep1Block extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (\Drupal::currentUser()->isAnonymous()) {
      return [];
    }
    $build = [];
    $markup = '<ol>';
    $markup .= '<li><b>Select indicator definition</b><p>Select \'create indicator state\' where state has not previously been created or \'update indicator state\' (shown beneath image) where state already exists.</p></li>';
    $markup .= '<li><b>Status narrative</b><p>The narrative should explain the current state and impact. Include a graph and or map illustrating theindicator state (can be linked to data portal).</p></li>';
    $markup .= '<li><b>Impact</b><p>Describe the impact the indicator state has on local resources, people and environment.</p></li>';
    $markup .= '<li><b>Response and Recommendations</b><p>Detail any current national actions, including policies and project development. Outline recommendations for further action.</p></li>';
    $markup .= '<li><b>Key Findings</b><p>Write a 3 to 4 sentence summary on the current status, impact and response.</p></li>';
    $markup .= '<li><b>Indicator State Classification</b><p>Classify the indicator state based on the trend of the indicator( up, down, mixed, neutral or unknown), the status (good, fair, poor) and confidence level.</p></li>';
    $markup .= '<li><b>Sources</b><p>Provide references / links to published and unpublished materials. Include links to the data portal where possible or other originating sources.</p></li>';
    $markup .= '<li><b>Review and Save</b></li>';
    $markup .= '</ol>';
    $build['update_indicator_state_step1_block']['#markup'] = $markup;
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['my_block_settings'] = $form_state->getValue('my_block_settings');
  }

}
