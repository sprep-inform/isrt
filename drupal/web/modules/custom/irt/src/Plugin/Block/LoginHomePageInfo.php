<?php

namespace Drupal\irt\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\File\Exception\FileException;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides a 'LoginHomePageInfo' block.
 *
 * @Block(
 *  id = "login_home_page_info",
 *  admin_label = @Translation("Login home page info"),
 * )
 */
class LoginHomePageInfo extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $markup = '<div class ="row">';
    $markup .= '<div class ="col-sm-7">';
    $markup .= '<h1>What is the Indicator Reporting Tool?</h1>';
    $markup .= '<p>The Indicator Reporting Tool aims to simplify reporting processes and reduce reporting burden by facilitating re-use of indicator definitions across multiple reporting obligations.</p>';
    $markup .= '<p>The tool can be used for multilateral reporting obligations such as MEAs (Multilateral Environmental Agreements) as well as national and state based indicator reporting.</p>';
    $markup .= '<p>The Indicator Reporting Tool is targetted towards government officials responsible for compiling and producing indicator based reports.</p>';
    $markup .= '</div>';
    $markup .= '<div class = "col-sm-5">';

    $image_path = '/var/www/html/drupal/web/modules/custom/irt/assets/images/home_page_block_image.png';
    $image_schema = 'public://home_page_block_image.png';
    /** @var \Drupal\Core\File\FileSystem $fs */
    $fs = \Drupal::service('file_system');
    if (!file_exists($image_schema)) {
      try {
        $fs->copy($image_path, 'public://home_page_block_image.png');
      }
      catch (FileException $e) {
        error_log(print_r(get_class($e), TRUE));
      }
    }
    $style = ImageStyle::load('large');
    $url = $style->buildUrl($image_schema);
    $image_link = \Drupal::service('file_url_generator')->generateAbsoluteString($url);
    $markup .= '<img src = "' . $image_link . ' ">';

    $markup .= '</div>';
    $markup .= '</div>';

    $build['login_home_page_info']['#markup'] = $markup;

    return $build;
  }

}
