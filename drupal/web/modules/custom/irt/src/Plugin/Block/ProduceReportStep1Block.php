<?php

namespace Drupal\irt\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a 'ProduceReportStep1Block' block.
 *
 * @Block(
 *  id = "produce_report_step_1_block",
 *  admin_label = @Translation("Produce Report Step 1 Block"),
 * )
 */
class ProduceReportStep1Block extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (\Drupal::currentUser()->isAnonymous()) {
      return [];
    }
    $build = [];
    $markup = t('<b>Step 1:</b>  Select reporting obligation.');
    $markup .= t('<br><br><b>Step 2:</b>  Review summary of indicator states and dates.');
    $markup .= t('<br><br><b>Step 3:</b>  Export full indicator states to editable document.');
    $build['produce_report_step_1_block']['#markup'] = $markup;
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['my_block_settings'] = $form_state->getValue('my_block_settings');
  }

}
