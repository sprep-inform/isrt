<?php

namespace Drupal\irt\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a 'ObligationHelpTextBlock' block.
 *
 * @Block(
 *  id = "obligation_help_text_block",
 *  admin_label = @Translation("Obligation Help text block"),
 * )
 */
class ObligationHelpTextBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (\Drupal::currentUser()->isAnonymous()) {
      return [];
    }
    $build = [];
    $markup = t('<b>Reporting Obligations:</b><br>  Reporting obligations are periodic national or international indicator based reports including Multilateral Environmental Agreements (MEAs), Sustainable Development Goals (SDGs), State of Environment Reports (SOEs), sector reports and ministerial reports.');
    $markup .= t('<br><br><b>Targets:</b><br> Every Reporting obligation has one or more targets, for example the Aichi targets under the Convention for Biodiversity (CBD).  Every target has one or more indicators, for example Aichi target 11 sets a 17% terrestrial area target and the terrestrial protected area indicator provides a measure for this target.');
    $markup .= t('<br><br><b>Indicator Definitions:</b><br>Indicator definitions are the specific descriptions of how an indicator is defined, the methodology to measure it, units of measurements etc');
    $markup .= t('<br><br><b>Indicator Definitions Mapping:</b><br>Indicator definition mapping defines the relationship between indicator definitions and  reporting obligation targets.');
    $markup .= t('<br><br>The power of the Indicator Reporting Tool is the reuse of the indicator definitions for multiple reporting obligations, thereby using a single indicator state for multiple reports.');
    $markup .= t('<br><br><I>Please consider using existing indicator definitions before developing new ones. </I>');
    $build['obligation_help_text_block']['#markup'] = $markup;
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['my_block_settings'] = $form_state->getValue('my_block_settings');
  }

}
