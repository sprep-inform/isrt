<?php

namespace Drupal\irt\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\file\Entity\File;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Provides a 'Group' block.
 *
 * @Block(
 *  id = "group",
 *  admin_label = @Translation("Group Block"),
 * )
 */
class GroupBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [];
    $image = '';
    $group_id = \Drupal::routeMatch()->getRawParameter('group');
    $group_id_parameter = \Drupal::routeMatch()->getRawParameters('arg_0');
    // To show block for edit forms get group from node_id.
    $node_id = \Drupal::routeMatch()->getRawParameter('node');

    if (!empty($group_id_parameter->get('arg_0'))) {
      $group_id_arg_0 = $group_id_parameter->all();
    }
    if (!empty($group_id) || !empty($group_id_arg_0) || !empty($node_id)) {
      // If the arg_0 has group id.
      if (!empty($group_id_arg_0['arg_0'])) {
        $group_id = $group_id_arg_0['arg_0'];
      }
      // Get group_id from node.
      elseif (empty($group_id) && !empty($node_id)) {
        $node = Node::load($node_id);
        // To get group id of node.
        $group_contents = GroupContent::loadByEntity($node);
        foreach ($group_contents as $group_content) {
          $group_id = $group_content->getGroup()->id();
        }
      }
      $markup = '<div class = "row group-block-row">';
      $markup .= '<div class = "col-sm-2 text-center">';
      $group = Group::load($group_id);
      $account = User::load(\Drupal::currentUser()->id());
      if ($group->getMember($account)) {
        if ($group) {
          // Get logo of group to show in block.
          $image_id = $group->field_group_logo->target_id;
          // Load image to get url of image to render in block.
          if ($image_id) {
            $file = File::load($image_id);
            if (!empty($file)) {
              $uri = $file->getFileUri();
              $image = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
            }
            if (!empty($image)) {
              $markup .= '<a href = "/group/' . $group_id . '/home" title = "Home"><img src = "' . $image . '" class="group-logo"> </a>';
            }
          }
          $markup .= '</div><div class ="col-sm-10 text-left "><b><h1 class = "group-home-label"><a href = "/group/' . $group_id . '/home" title = "Home">' . $group->label() . '</a> <br> Indicator Reporting Tool </h1></b></div>';
          $markup .= '</div>';
          $build['group_block']['#markup'] = $markup;

        }
        else {
          $markup = '<div class = "col-sm-12 text-center"> <h2> Indicator Reporting Tool </h2></div>';
          $build['group_block_title']['#markup'] = $markup;
        }
      }
      else {
        $markup = '<div class = "col-sm-12 text-center"> <h2> Indicator Reporting Tool </h2></div>';
        $build['group_block_title']['#markup'] = $markup;
      }

    }
    else {
      $markup = '<div class = "col-sm-12 text-center"> <h2> Indicator Reporting Tool </h2></div>';
      $build['group_block_title']['#markup'] = $markup;
    }
    return $build;

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
