<?php

namespace Drupal\irt\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\OptGroup;
use Drupal\group\Entity\GroupContent;
use Drupal\node\Entity\Node;

/**
 * Plugin implementation of the 'target_integer_widget' widget.
 *
 * Only works for field_indicator_assessment in topic nodes.
 *
 * @FieldWidget(
 *   id = "target_integer_widget",
 *   label = @Translation("Target Integer Widget"),
 *   field_types = {
 *     "related_target_integer"
 *   },
 *   multiple_values = FALSE
 * )
 */
class RelatedTargetWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['value'] = [
      '#type' => 'select',
      '#options' => $this->getOptions($items->getEntity()),
      '#default_value' => NULL,
          // Do not display a 'multiple' select box if there is only one option.
      '#multiple' => FALSE,
    ];

    return $element;
  }

  /**
   * Get options.
   */
  public function getOptions(FieldableEntityInterface $entity) {

    // Return parent::getOptions($entity);
    $group = '';
    $options1 = NULL;
    $node = $entity;
    if (!$entity->isNew()) {
      $group_contents = GroupContent::loadByEntity($node);
      foreach ($group_contents as $group_content) {
        $group = $group_content->getGroup();
      }
    }
    if ($group) {
      // $this->options[0] = '- None -';
      $options = [NULL => 'NONE-'];
      foreach ($group->getContent('group_node:obligation') as $obligation_group_content) {
        $obligation_id = $obligation_group_content->entity_id->target_id;
        $obligation_node = Node::load($obligation_id);
        if ($obligation_node) {
          foreach ($obligation_node->get('field_target') as $target) {
            if (isset($target->entity)) {
              $target_id = $target->entity->id();
              $target_node = Node::load($target_id);
              if ($target_node) {
                $options[$obligation_node->label()][$target_id] = $target_node->label();
                $module_handler = \Drupal::moduleHandler();
                $context = [
                  'fieldDefinition' => $this->fieldDefinition,
                  'entity' => $entity,
                ];
                $module_handler->alter('options_list', $options, $context);

                // Options might be nested ("optgroups").
                // If the widget does not support
                // nested options, flatten the list.
                if (!$this->supportsGroups()) {
                  $options = OptGroup::flattenOptions($options);
                }

                $options1 = $options;
              }
            }
          }

        }
      }
    }

    return $options1;
  }

  /**
   * Indicates whether the widgets support optgroups.
   *
   * @return bool
   *   TRUE if the widget supports optgroups, FALSE otherwise.
   */
  protected function supportsGroups() {
    return TRUE;
  }

}
