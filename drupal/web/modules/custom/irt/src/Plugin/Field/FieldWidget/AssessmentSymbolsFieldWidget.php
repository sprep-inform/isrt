<?php

namespace Drupal\irt\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'assessment_symbols_field_widget' widget.
 *
 * Only works for field_indicator_assessment in topic nodes.
 *
 * @FieldWidget(
 *   id = "assessment_symbols_field_widget",
 *   label = @Translation("Assessment symbols field widget"),
 *   field_types = {
 *     "list_string"
 *   },
 *   multiple_values = TRUE
 * )
 */
class AssessmentSymbolsFieldWidget extends OptionsSelectWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'size' => 60,
      'placeholder' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['box'] = [
      '#type' => 'fieldset',
      '#title' => 'Indicator State',
    ];
    $is_new = TRUE;
    $option = [];
    if (!empty($items->getValue())) {
      $is_new = FALSE;
      foreach ($items->getValue() as $value) {
        [$option['status'], $option['trend'], $option['confidence']] = explode('-', $value['value']);
      }
      if ($option['status'] == 'gtf') {
        $option['status'] = 'Good to fair';
      }
      if ($option['status'] == 'ftp') {
        $option['status'] = 'Fair to poor';
      }
    }

    $values = $this->getDefinedValues();
    $status_options = $this->getDefinedOptions($values['status']);
    $trend_options = $this->getDefinedOptions($values['trend']);
    $confidence_options = $this->getDefinedOptions($values['confidence']);

    $element['box']['status'] = [
      '#type' => 'select',
      '#options' => $status_options,
      '#default_value' => $is_new ? '_none' : $option['status'],
      '#title' => 'Status',
      '#description' => 'Select an appropriate status based on available data and defined baselines.',
    ];
    $element['box']['trend'] = [
      '#type' => 'select',
      '#options' => $trend_options,
      '#default_value' => $is_new ? '_none' : $option['trend'],
      '#title' => 'Trend',
      '#description' => 'Select the trend based on the direction towards the desired outcome.',
    ];
    $element['box']['confidence'] = [
      '#type' => 'select',
      '#options' => $confidence_options,
      '#default_value' => $is_new ? '_none' : $option['confidence'],
      '#title' => 'Confidence',
      '#description' => 'Select the confidence level based on temporal and geographic coverage; and any known data deficiencies.',
    ];
    return $element;
  }

  /**
   * Get getDefinedValues.
   *
   * @return array|mixed
   *   Return array.
   */
  private function getDefinedValues() {
    $values = $this->fieldDefinition->getFieldStorageDefinition()
      ->getSettings()['allowed_values'];
    $status = [];
    $trend = [];
    $confidence = [];
    foreach ($values as $key => $value) {
      [$status[], $trend[], $confidence[]] = explode('-', $key);
    }
    $status = array_merge(array_unique($status));
    $trend = array_merge(array_unique($trend));
    $confidence = array_merge(array_unique($confidence));
    $values = [
      'status' => $status,
      'trend' => $trend,
      'confidence' => $confidence,
    ];
    return $values;
  }

  /**
   * Helper function to get a list of defined options.
   *
   * @param array $values
   *   Passing array of values.
   *
   * @return array
   *   Returns array of computed strings.
   */
  private function getDefinedOptions(array $values) {
    $options = [];
    $options['_none'] = '- None -';

    foreach ($values as $value) {
      if ($value == 'gtf') {
        $options['Good to fair'] = 'Good to fair';
      }
      elseif ($value == 'ftp') {
        $options['Fair to poor'] = 'Fair to poor';
      }
      else {
        $options[$value] = ucwords($value);
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if ($element['status'] || $element['trend'] || $element['confidence']) {
      $form_state->setErrors();
    }
    parent::validateElement($element, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    if ($values['box']['status'] == 'Good to fair') {
      $values['box']['status'] = 'gtf';
    }
    if ($values['box']['status'] == 'Fair to poor') {
      $values['box']['status'] = 'ftp';
    }
    $values['value'] = $values['box']['status'] . '-' . $values['box']['trend'] . '-' . $values['box']['confidence'];
    return parent::massageFormValues($values, $form, $form_state);
  }

}
