<?php

namespace Drupal\irt\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'assessment_symbols_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "assessment_symbols_field_formatter",
 *   label = @Translation("Assessment symbols field formatter"),
 *   field_types = {
 *     "list_string"
 *   }
 * )
 */
class AssessmentSymbolsFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $data = [];
    $image_link = '';
    /**
     * @var \Drupal\Core\Field\FieldItemInterface $item
     */
    foreach ($items as $delta => $item) {
      $value = $item->getValue()['value'];
      if (!empty($value)) {
        [$data['state'], $data['trend'], $data['confidence']] = explode('-', $value);
        $image_schema = 'public://' . $data['state'] . '_' . $data['trend'] . '_' . $data['confidence'] . '.png';
        $type = pathinfo($image_schema, PATHINFO_EXTENSION);
        if (file_exists($image_schema)) {
          $data = file_get_contents($image_schema);
          // $style = ImageStyle::load('medium');
          // $url =  $style->buildUrl($image_schema);
          $image_link = 'data:image/' . $type . ';base64,' . base64_encode($data);
        }
        else {
          $image_link = t('Image missing-database');
        }
      }
      else {
        $image_link = t('Image missing-value not selected');
      }
    }

    if (empty($data)) {
      $image_link = t('Image missing-value not selected');
    }
    $formattedDate = NULL;
    $date = $items->getEntity()->field_year_valid->date;
    if ($date) {
      $formattedDate = $date->format('Y');
    }
    $elements = [
      0 =>
        [
          '#theme' => 'symbols',
          '#data' => $data,
          '#image' => $image_link,
          '#url' => $items->getEntity()->toUrl(),
          // \Drupal::service('path.alias_manager')->getAliasByPath('/node/' .
          // $items->getEntity()->id()).
          '#date' => $formattedDate,
        ],
    ];
    return $elements;
  }

}
