<?php

namespace Drupal\irt\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldWidget\DateTimeDatelistWidget;

/**
 * Plugin implementation of the 'year_field_widge' widget.
 *
 * @FieldWidget(
 *   id = "year_field_widge",
 *   label = @Translation("Year field widge"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class YearFieldWidge extends DateTimeDatelistWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $date_part_order = ['year'];
    $increment = parent::getSetting('increment');

    $element['value'] = [
      '#type' => 'datelist',
      '#date_increment' => $increment,
      '#date_part_order' => $date_part_order,
    ] + $element['value'];

    return $element;
  }

}
