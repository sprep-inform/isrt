<?php

namespace Drupal\irt\Plugin\Field;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\group\Entity\GroupContent;
use Drupal\node\Entity\Node;

/**
 * Class ObligationsAndTargets.
 *
 * This computed field should live on indicator_definition content type. It will
 * render the related obligations and targets for a state definition.
 *
 * @package Drupal\irt\Plugin\Field
 */
class ObligationsAndTargets extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $markup = '';
    $obligation_targets = [];
    if ($this->getName() == 'obligations_and_targets') {
      $entity = $this->getEntity();
      $indicator_definition_id = 0;
      switch ($entity->bundle()) {
        case 'indicator_definition';
          $indicator_definition_id = $entity->id();
          break;

        case 'indicator_state';
          $indicator_definition_id = $entity->field_state_indicator_definition->target_id;
          break;
      }
      /** @var \Drupal\node\NodeStorage $node_storage */
      $node_storage = \Drupal::entityTypeManager()->getStorage('node');
      /** @var \Drupal\Core\Entity\Query\Sql\Query $entityQuery */
      $entityQuery = $node_storage->getQuery();
      $entityQuery->condition('type', 'target');
      $entityQuery->condition('status', '1');
      $entityQuery->condition('field_indicator_definition', $indicator_definition_id);
      $entityQuery->accessCheck(TRUE);
      // "3" => "3".
      $target_node_ids = $entityQuery->execute();
      foreach ($target_node_ids as $target_node_id) {
        // Fetching obligation_node_ids.
        /** @var \Drupal\node\NodeStorage $node_storage */
        $node_storage = \Drupal::entityTypeManager()->getStorage('node');
        /** @var \Drupal\Core\Entity\Query\Sql\Query $entityQuery */
        $entityQuery = $node_storage->getQuery();
        $entityQuery->condition('type', 'obligation');
        $entityQuery->condition('status', '1');
        $entityQuery->condition('field_target', $target_node_id);
        $entityQuery->accessCheck(TRUE);
        $obligation_node_ids = $entityQuery->execute();
        foreach ($obligation_node_ids as $obligation_node_id) {
          $obligation_targets[$obligation_node_id][] = $target_node_id;
        }
      }
      $route_group_id = \Drupal::routeMatch()->getRawParameter('group');
      $parameter_arg_0 = \Drupal::routeMatch()->getRawParameter('arg_0');
      if ($route_group_id == NULL && $parameter_arg_0 != NULL) {
        $route_group_id = $parameter_arg_0;
      }
      if ($route_group_id) {
        // Render the list.
        $markup .= '<ul>';
        foreach ($obligation_targets as $obligation => $targets) {
          $obligation_node = Node::load($obligation);
          // To get group of obligation node.
          $group_contents = GroupContent::loadByEntity($obligation_node);
          foreach ($group_contents as $group_content) {
            $group_id = $group_content->getGroup()->id();
          }
          if ($route_group_id == $group_id) {
            $markup .= '<li> <a href ="/irt/' . $group_id . '/obligation/' . $obligation_node->id() . '">' . $obligation_node->getTitle() . '</a></li>';
            $target_nodes = Node::loadMultiple($targets);
            if ($target_nodes && count($target_nodes) > 0) {
              $markup .= '<ul>';
              foreach ($target_nodes as $target_node) {
                $markup .= '<li>' . $target_node->getTitle() . '</li>';
              }
              $markup .= '</ul>';
            }
          }
        }
        $markup .= '</ul>';
        $this->setValue(new FormattableMarkup($markup, []));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
