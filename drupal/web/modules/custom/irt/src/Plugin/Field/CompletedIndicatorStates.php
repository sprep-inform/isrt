<?php

namespace Drupal\irt\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\node\Entity\Node;

/**
 * Plugin implementation for calculating indicating states completed per group.
 */
class CompletedIndicatorStates extends FieldItemList {
  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    if ($this->getName() == 'completed_indicator_states') {
      $entity = $this->getEntity();
      if ($entity->bundle() == 'obligation') {
        $target_ids = array_column($entity->field_target->getValue(), 'target_id');
        $indicator_definition_ids = [];
        foreach ($target_ids as $target_id) {
          $target = Node::load($target_id);
          $indicator_definition_ids[] = $target->field_indicator_definition->target_id;
        }
        $indicator_definition_ids = array_unique($indicator_definition_ids);
        $indicator_state_ids = [];
        foreach ($indicator_definition_ids as $indicator_definition_id) {
          $query = \Drupal::entityQuery('node')
            ->condition('type', 'indicator_state')
            ->condition('status', 1)
            ->condition('field_state_indicator_definition.entity.nid', $indicator_definition_id)
            ->accessCheck(TRUE);
          $id = $query->execute();
          if (!empty($id)) {
            $indicator_state_ids[] = $id;
          }
        }
        $this->setValue(count($indicator_state_ids) . '/' . count($indicator_definition_ids));
      }
    }
  }

}
