<?php

namespace Drupal\irt\Plugin\views\field;

use Drupal\node\Entity\Node;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to flag the node type.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("completed_indicator_states")
 */
class CompletedIndicatorStatesComputedField extends FieldPluginBase {

  /**
   * The query function.
   *
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * The render function.
   *
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $node = $values->_entity;
    if ($node->bundle() == 'obligation') {
      $target_ids = array_column($node->field_target->getValue(), 'target_id');
      $indicator_definition_ids = [];
      foreach ($target_ids as $target_id) {
        $target = Node::load($target_id);
        if (!empty($target)) {
          $indicator_definitions = $target->get('field_indicator_definition');
          /** @var \Drupal\node\Entity\Node $indicator_definition */
          foreach ($indicator_definitions->referencedEntities() as $indicator_definition) {
            $indicator_definition_groups_ids = [];
            $indicator_definition_ids[] = $indicator_definition->id();
          }
        }
      }
      $indicator_definition_ids = array_unique($indicator_definition_ids);
      $indicator_state_ids = [];
      foreach ($indicator_definition_ids as $indicator_definition_id) {
        $query = \Drupal::entityQuery('node')
          ->condition('type', 'indicator_state')
          ->condition('status', 1)
          ->condition('field_state_indicator_definition.entity.nid', $indicator_definition_id)
          ->accessCheck(TRUE);
        $id = $query->execute();
        if (!empty($id)) {
          $indicator_state_ids[] = $id;
        }
      }
      $result = count($indicator_state_ids) . '/' . count($indicator_definition_ids);
      return $result;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
