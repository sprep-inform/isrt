<?php

namespace Drupal\irt\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupContent;
use Drupal\irt\UtilityTrait;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Views;

/**
 * Filters by given list of node title options.
 *
 * Works for create_update_indicator_state view.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("obligation_filter_2")
 */
class ObligationFilter2 extends FilterPluginBase {

  use UtilityTrait;

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $form['value'] = [
      '#empty_option' => t('- Any -'),
      '#empty_value' => 'All',
      '#type' => 'select',
      '#title' => t('Obligations'),
      '#options' => $this->getObligations(),
      '#attributes' => [
        'class' => ['views-obligation_filter-select'],
      ],
    ];
  }

  /**
   * Helper to print current query String.
   *
   * @return string
   *   The query string.
   */
  private function getQueryString() {
    return (string) $this->query->query();
  }

  /**
   * Loads obligation nids and labels from DB.
   *
   * @return array
   *   Return select list option list.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getObligations() {
    $options = [];
    $storage = \Drupal::entityTypeManager()->getStorage('node');
    $obligation_nodes = $storage->loadByProperties(['type' => 'obligation']);
    $group_id_from_route = \Drupal::routeMatch()->getRawParameter('group');

    foreach ($obligation_nodes as $nid => $node) {
      $group_content = GroupContent::loadByEntity($node);
      foreach ($group_content as $content) {
        $group_id = $content->getGroup()->id();
      }
      if ($group_id == $group_id_from_route) {
        $options[$nid] = $node->label();
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $query = $this->query;
    // Drupal\views\Plugin\ViewsHandlerManager.
    // Drupal\views\Plugin\views\join\Standard.
    // First join.
    $join = Views::pluginManager('join')
      ->createInstance('standard', [
        'type' => 'LEFT',
        'table' => 'node__field_indicator_definition',
        'field' => 'field_indicator_definition_target_id',
        'left_table' => 'node_field_data',
        'left_field' => 'nid',
      ]);
    $query->addRelationship('node__field_indicator_definition', $join, 'node_field_data');

    // Second join.
    $join = Views::pluginManager('join')
      ->createInstance('standard', [
        'type' => 'LEFT',
        'table' => 'node__field_target',
        'field' => 'field_target_target_id',
        'left_table' => 'node__field_indicator_definition',
        'left_field' => 'entity_id',
      ]);
    $query->addRelationship('node__field_target', $join, 'node__field_indicator_definition');

    // Third join.
    $input = $this->view->getExposedInput();
    $join = Views::pluginManager('join')
      ->createInstance('standard', [
        'type' => 'INNER',
        'table' => 'node_field_data',
        'field' => 'nid',
        'left_table' => 'node__field_target',
        'left_field' => 'entity_id',
        'extra' => [
          0 => [
            'field' => 'nid',
            'value' => $input['obligation_filter_2'],
          ],
        ],
      ]);
    $query->addRelationship('node_field_data_obligation', $join, 'node__field_target');

    // Set distinct to true.
    $query->distinct = TRUE;
  }

}
