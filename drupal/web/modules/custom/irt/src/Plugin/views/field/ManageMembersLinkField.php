<?php

namespace Drupal\irt\Plugin\views\field;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\group\Entity\Group;
use Drupal\user\Entity\User;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to flag the node type.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("manage_members_link")
 */
class ManageMembersLinkField extends FieldPluginBase {

  /**
   * The query function.
   *
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * The render function.
   *
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $node = $values->_entity;
    if ($node->bundle() == 'countries') {
      // Load current user.
      $user = User::load(\Drupal::currentUser()->id());
      // Load group node.
      $group = Group::load($node->id());
      // Get group member from user.
      $member = $group->getMember($user);
      if ($member->hasPermission('administer members')) {
        $url = Url::fromRoute('view.group_members.page_1', [
          'group' => $group->id(),
        ], ['absolute' => TRUE]);

        $link = Link::fromTextAndUrl('Manage members ', $url)->toString();
        return $link;
      }
      else {
        return NULL;
      }
    }

  }

}
