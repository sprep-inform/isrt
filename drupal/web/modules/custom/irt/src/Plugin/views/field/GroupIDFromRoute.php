<?php

namespace Drupal\irt\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to provides the group id of target.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("group_id_from_route")
 */
class GroupIDFromRoute extends FieldPluginBase {

  /**
   * The query function.
   *
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * The render function.
   *
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $route_group_id = \Drupal::routeMatch()->getRawParameter('group');
    if ($route_group_id) {
      return $route_group_id;
    }
  }

}
