<?php

namespace Drupal\irt\Plugin\views\field;

use Drupal\group\Entity\GroupContent;
use Drupal\node\Entity\Node;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to provides the group id of target.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("group_id_of_target")
 */
class GroupIdTargetField extends FieldPluginBase {

  /**
   * The query function.
   *
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * The render function.
   *
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $target_id = $values->field_target_node_field_data_nid;
    if ($target_id) {
      $target_node = Node::load($target_id);
      foreach (GroupContent::loadByEntity($target_node) as $group_content) {
        $group_id = $group_content->getGroup()->id();
      }
      if ($group_id) {
        return $group_id;
      }
    }
  }

}
