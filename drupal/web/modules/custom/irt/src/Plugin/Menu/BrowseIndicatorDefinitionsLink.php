<?php

namespace Drupal\irt\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;
use Drupal\Core\Menu\StaticMenuLinkOverridesInterface;
use Drupal\group\Context\GroupRouteContextTrait;
use Drupal\group\Entity\GroupContent;
use Drupal\node\Entity\Node;

/**
 * Represents a menu link for browse_indicator_definitions view.
 */
class BrowseIndicatorDefinitionsLink extends MenuLinkDefault {

  use GroupRouteContextTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, StaticMenuLinkOverridesInterface $static_override) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $static_override);
    $group = $this->getGroupFromRoute();
  }

  /**
   * Represents a menu link for a user profile edition.
   */
  public function getRouteParameters() {
    // Get the group parameter from Url.
    // For some reason, on Produce Indicator State Sheet page
    // group_id is stored in arg_0.
    $group_arg = \Drupal::routeMatch()->getRawParameter('arg_0');
    $group_id = \Drupal::routeMatch()->getRawParameter('group');
    $nid = \Drupal::routeMatch()->getRawParameter('node');

    if (!empty($group_id)) {
      return ['arg_0' => $group_id];
    }
    elseif (!empty($group_arg)) {
      return ['arg_0' => $group_arg];
    }
    elseif (!empty($nid)) {
      $node = Node::load($nid);
      // To get group id of node.
      $group_contents = GroupContent::loadByEntity($node);
      $group_id_from_nid = 0;
      foreach ($group_contents as $group_content) {
        $group_id_from_nid = $group_content->getGroup()->id();
      }
      return ['arg_0' => $group_id_from_nid];
    }
    // If user is anonymous.
    else {
      return ['arg_0' => '0'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
