<?php

namespace Drupal\eopts\Plugin\Devel\Dumper;

use Drupal\devel\Plugin\Devel\Dumper\VarDumper;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\HtmlDumper as SymfonyHtmlDumper;

/**
 * Provides a Symfony VarDumper dumper plugin.
 *
 * @DevelDumper(
 *   id = "html_dumper",
 *   label = @Translation("HTML var-dumper"),
 *   description = @Translation("A custom HTML dumper, can be helpful when using CLI or AJAX callbacks."),
 * )
 */
class HtmlDumper extends VarDumper {

  /**
   * Provides and HTML dumper.
   *
   * @see VarDumper::export()
   */
  public function exportHtml($input, $name = NULL) {
    $cloner = new VarCloner();
    $dumper = new SymfonyHtmlDumper();

    $output = fopen('php://memory', 'r+b');
    $dumper->dump($cloner->cloneVar($input), $output);
    $output = stream_get_contents($output, -1, 0);

    if ($name) {
      $output = $name . ' => ' . $output;
    }

    return $this->setSafeMarkup($output);
  }

}
