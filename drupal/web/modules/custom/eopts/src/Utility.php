<?php

namespace Drupal\eopts;

/**
 * Trait Utility logs kin to the Drupal root.
 */
class Utility {

  /**
   * Helper function to log kint.html to the Drupal root.
   *
   * @param mixed $mixed
   *   Anything.
   * @param string $destination
   *   The file destination, default is /tmp/ .
   */
  public static function varDump($mixed, $destination = '') {
    \Drupal::service('eopts.commands')->varDump($mixed, $destination);
  }

}
