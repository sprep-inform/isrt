<?php

namespace Drupal\eopts\EventSubscriber;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\Settings;
use Drupal\jsonapi\Routing\Routes;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Subscribe to jsonapi request events.
 */
class JsonApiRequestBlockFailedBasicAuth implements EventSubscriberInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  private $routeMatch;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $currentUser;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * Inject required services.
   */
  public function __construct(RouteMatchInterface $current_route_match, AccountInterface $current_user, ModuleHandlerInterface $module_handler) {
    $this->routeMatch = $current_route_match;
    $this->currentUser = $current_user;
    $this->moduleHandler = $module_handler;

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];

    $events[KernelEvents::REQUEST] = ['onRequest'];

    return $events;
  }

  /**
   * Respond to jsonapi + basic auth requests.
   */
  public function onRequest(RequestEvent $event): void {

    // Allow to turn this behavior off if needed.
    $disabled = Settings::get('eopts')['jsonapi_hardening']['disabled'] ?? FALSE;
    if ($disabled) {
      return;
    }

    // This only applies for jsonapi and basic auth.
    if (!($this->moduleHandler->moduleExists('jsonapi') && $this->moduleHandler->moduleExists('basic_auth'))) {
      return;
    }

    $is_json_api_request = $this->routeMatch->getRouteObject()->hasDefault(Routes::JSON_API_ROUTE_FLAG_KEY);
    if (!$is_json_api_request) {
      return;
    }

    $basic_auth = \Drupal::service('basic_auth.authentication.basic_auth');
    if (!$basic_auth->applies($event->getRequest())) {
      return;
    }

    // If this is a jsonapi request, then throw an access denied
    // exception if the user provided credentials, but failed
    // to authenticate.
    if (!$this->currentUser->isAuthenticated()) {
      throw new AccessDeniedHttpException("Access Denied: Invalid Credentials");
    }

  }

}
