# Eopts module functionality

## Email monitoring:
Adding these values to settings.local.php will trigger succeeded email monitoring:

```
// Succeeded email monitoring
$settings['succeeded_mail']['email'] = 'testemail@succeeded.eightyoptions.com';
$settings['succeeded_mail']['api_key'] = '123';
```

More details here: https://eightyoptions.atlassian.net/l/cp/phXfkjf7

## HTML var dumper (debugger)
You can use the following script to debug code into a var_dumper html format. Basically it can be used similarly to kint or ksm, but this tool is more useful when running command-line commands or ajax commands, where it is challenging to get a kint or ksm output.

Add the following debug statement `\Drupal\eopts\Utility::varDump($mixed);`, then look for the `var_dump.html` file under drupal web root, e.g: http://nginx/var_dump.html


## JsonAPI Permission Hardening.

By default, if a user fails to authenticate using basic auth on a json api request, they will continue
to see content under the anonymous user context. This module will ensure that these requests will return
an "Access Denied" error if the passed in username and password fail to authenticate. Users can still
access jsonapi anonymously if they don't provide any credentials.

To disable this functionality, set:

```php
$settings['eopts']['jsonapi_hardening']['disabled'] = TRUE;
```
