<?php

namespace Drupal\Tests\eopts\Functional;

use Drupal\Component\Serialization\Json;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Test the jsonapi + basic auth behavior of the eopts module.
 */
class JsonApiHardeningTest extends BrowserTestBase {

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();
    $user = User::load(1);
    $user->setPassword("password")->save();
  }

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'basic_auth',
    'jsonapi',
    'eopts',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test that anonymous user has access to jsonapi.
   *
   * So long as they don't supply a username and password.
   */
  public function testAnonJsonApi() {
    // Disabling htmlOutput: strange things happening with Guzzle reading the
    // stream into the htmlOutput instead of the request response.
    $this->htmlOutputEnabled = FALSE;

    $jsonapi_base_path = \Drupal::getContainer()->getParameter('jsonapi.base_path');
    $response = $this->buildHttpClient()->request('GET', $jsonapi_base_path);
    $this->assertSame(200, $response->getStatusCode());
    $data = Json::decode($response->getBody()->getContents());
    $this->assertNotEmpty($data['links']);
  }

  /**
   * Test that authenticated user has access to jsonapi.
   */
  public function testCorrectAuthJsonApi() {
    // Disabling htmlOutput: strange things happening with Guzzle reading the
    // stream into the htmlOutput instead of the request response.
    $this->htmlOutputEnabled = FALSE;

    $jsonapi_base_path = \Drupal::getContainer()->getParameter('jsonapi.base_path');
    $password = \Drupal::service('password_generator')->generate();
    $this->drupalCreateUser([], 'test', FALSE, ['pass' => $password]);

    $response = $this->buildHttpClient([
      'auth' => [
        'test',
        $password,
        'basic',
      ],
    ])->request('GET', $jsonapi_base_path);

    assert($response instanceof ResponseInterface);
    $this->assertSame(200, $response->getStatusCode());
    $data = Json::decode($response->getBody()->getContents());
    $this->assertNotEmpty($data['links']);
  }

  /**
   * Test that anonymous user has access to jsonapi.
   */
  public function testFailedAuthJsonApi() {
    // Disabling htmlOutput: strange things happening with Guzzle reading the
    // stream into the htmlOutput instead of the request response.
    $this->htmlOutputEnabled = FALSE;

    $jsonapi_base_path = \Drupal::getContainer()->getParameter('jsonapi.base_path');
    $response = NULL;
    try {
      $this->buildHttpClient([
        'auth' => [
          'wrong',
          'wrong',
          'basic',
        ],
      ])->request('GET', $jsonapi_base_path);
    }
    catch (ClientException $ex) {
      $response = $ex->getResponse();
    }
    assert($response instanceof ResponseInterface);
    $this->assertSame(403, $response->getStatusCode());
    $data = Json::decode($response->getBody()->getContents());
    $this->assertEquals("Access Denied: Invalid Credentials", $data['errors'][0]['detail']);
  }

  /**
   * Account basic details should not be visible publicly.
   */
  public function testAnonymousAccountDetailsAccess() {
    $jsonapi_base_path = \Drupal::getContainer()->getParameter('jsonapi.base_path');
    $this->drupalGet("$jsonapi_base_path/user/user");

    $web_assert = $this->assertSession();
    $web_assert->statusCodeEquals(200);
    // Full name / display name.
    $web_assert->pageTextNotContains('"name":"admin"');
    $web_assert->pageTextNotContains('"display_name":"admin"');
    $web_assert->pageTextNotContains('"mail":"admin@example.com"');
    $web_assert->pageTextContainsOnce('"display_name":"Anonymous"');
    $web_assert->pageTextContains('"display_name":"Restricted"');

    // Super admin, make sure those values exists.
    $user = User::load(1);
    $user->passRaw = "password";
    $this->drupalLogin($user);
    $this->drupalGet("$jsonapi_base_path/user/user");
    $web_assert = $this->assertSession();
    $web_assert->statusCodeEquals(200);
    $web_assert->pageTextContains('"name":"admin"');
    $web_assert->pageTextContains('"display_name":"admin"');
    $web_assert->pageTextContains('"mail":"admin@example.com"');
    $web_assert->pageTextContainsOnce('"display_name":"Anonymous"');
    $web_assert->pageTextNotContains('"display_name":"Restricted"');
  }

  /**
   * Build an http (guzzle) client for curl requests.
   */
  private function buildHttpClient($options = []) : ClientInterface {
    $options['base_uri'] = $this->baseUrl;
    return $this->container->get('http_client_factory')->fromOptions($options);
  }

}
