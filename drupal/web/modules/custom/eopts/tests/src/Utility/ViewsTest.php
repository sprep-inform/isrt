<?php

namespace Drupal\Tests\eopts\Utility;

use Drupal\views\Entity\View;
use weitzman\DrupalTestTraits\ExistingSiteBase;

/**
 * Eopts test class for views objects.
 *
 * Note that this test requires the website to be built and ready.
 *
 * @group existing_site
 */
class ViewsTest extends ExistingSiteBase {

  /**
   * Inspect all current views objects for any broken handlers.
   */
  public function testInspectBrokenHandlers() {
    // Make sure views module is enabled.
    if (!\Drupal::moduleHandler()->moduleExists('views')) {
      // Just do an assertion so the tests won't fail when we return.
      $this->assertTrue(TRUE);
      return;
    }
    $view_ids = View::loadMultiple();
    foreach ($view_ids as $view_id => $view) {
      if (!$view->status()) {
        // Ignore disabled views.
        continue;
      }
      // Get view displays, e.g: default, page_1, block_1.
      $displays = $view->get('display');
      $executable = $view->getExecutable();
      $executable->initDisplay();
      $handler_types = $executable->getHandlerTypes();
      // $handler_types_id, e.g: field, sort, filter, relationship etc ..
      $handler_types_ids = array_keys($handler_types);
      foreach ($displays as $display_id => $display) {
        foreach ($handler_types_ids as $handler_type_id) {
          /** @var \Drupal\views\Plugin\views\display\DisplayPluginBase $display */
          $display_handler = $executable->displayHandlers->get($display_id);
          // We need field_id (field machine name). eg: status, title,
          // created etc ...
          foreach ($display_handler->getOption($handler_types[$handler_type_id]['plural']) as $field_id => $field_detail) {
            /** @var \Drupal\views\Plugin\views\field\FieldPluginBase $views_field_handler */
            $views_field_handler = $display_handler->getHandler($handler_type_id, $field_id);
            if ($views_field_handler) {
              $this->assertFalse($views_field_handler->broken(), "A broken views handler detected. view_id/display: $view_id/$display_id, type: $handler_type_id, field: $field_id");
            }
          }
        }
      }
    }
  }

}
