<?php

namespace Drupal\Tests\eopts\Utility;

use weitzman\DrupalTestTraits\ExistingSiteBase;

/**
 * Test anonymous user disable create new account.
 *
 * @group existing_site
 */
class AnonymousDisableCreateAccountTest extends ExistingSiteBase {

  /**
   * Check is the anonymous user able to create a new account.
   */
  public function testAnonymousDisableCreateAccountTest() {
    $web_assert = $this->assertSession();

    // Go to login page.
    $this->drupalGet('/user/login');
    $web_assert->statusCodeEquals(200);

    // Ensure register page is not accessible to anon user.
    $this->drupalGet('/user/register');
    $this->assertSession()->statusCodeEquals(403);
  }

}
