# SPREP INFORM Indicator Reporting Tool

## Getting started with local development

This project comes with local development environment tools based on the docker4drupal project.

Run ```docker-compose up -d``` to get started.

Then, from within the php container install all the dependencies such as drupal core and contrib modules using composer.

```
docker-compose exec php sh
cd drupal
composer install
```

Then install the site.

`composer build`

You should now be able to access the site from your browser at `indicator-reporting-tool.localhost`